/* example code for Diver */

class A {
	static int g;
	float d=1.5f;
	protected short o;

	String M1(int[] f, int z) /*throws Exception*/ {
		int x = f[0] + z;
		int y = 2;
		int c1 = 5, c2 = 0;

		/*
		if (x*y == 0) {
			throw new Exception("invalid incoming parameter.");
		}
		*/
		// will thrown an exception when x*y==0
		System.out.println("test validity of incoming parameter: " + c1/(x*y));

		if (x > y) {
			M2(x,y);
		}
		else {
			M2(c1,c2);
		}

		//int h = c1 + c2*10;
		int h = 21;
		B b = new B();
		int r = b.M3(h,g);

		String s = "doneInM1-";
		s += r;
		return s;
	}

	void M2(int m, int n) {
		float t = m + d;

		if (t > 0) {
			g = Math.max((int)t,n);
		}

		B[] p = new B[]{ new B() };

		boolean b = false;
		b = C.M5(p);
		System.out.println(b);
	}
}

