import profile.BranchReporter;
import profile.DUAReporter;
import Diver.EAMonitor;
import EAS.Monitor;

/* example code for Diver */

class C {
	static void __link() { 
		BranchReporter.__link(); DUAReporter.__link(); 
		Monitor.__link(); EAMonitor.__link(); 
	}
	
	public static boolean M5(B[] q) {
		long y = q[0].n;
		boolean b = B.l > y;

		D[] d = new D[]{new D()};
		A[] a = (D[])d;

		// the following would cause ArraryStoreException
		//a[0] = new A();
		// note that downcasting is impermissible
		//a[0] = (D)(new A());
		a[0] = new D();
		D u = d[0];
		u.toString();

		return b;
	}

	char M6(B[] x, char y) {
		M5(x);
		return y;
	}

	public static void main(String[] args) {
		int[] a = new int[]{0,-1,2};
		int b = -3;

		A w = new A();
		String s = w.M1(a,b);

		double d = B.M4();
		String u = s + d;
		System.out.println(u);
	}
}

