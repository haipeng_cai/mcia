
import java.io.*;
import java.util.*;

public class multiCatchBlock
{
	static int ABC = 100;
	public static void main(String[] args) {
		int c = 1/1;

		try {
			int a = 1/0;
		}
		catch (Exception e) {
			System.out.println("the first catch block.");
			e.printStackTrace();
		}
		finally {
			System.out.println("the first finally block.");
		}

		try {
			int b = 2/1;
		}
		catch (Exception e) {
			System.out.println("the second catch block.");
			e.printStackTrace();
		}
		finally {
			System.out.println("the second finally block.");
		}

		System.out.println("program ended.");
	}

}

