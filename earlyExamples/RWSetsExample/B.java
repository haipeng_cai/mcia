package examples;

public class B {
  
  int z;
  
  public void n(A b) {
	A a = b;
    z = a.x;
    if (a.y > 10) {
      System.out.println("HELLO");
    }

	m1(z, a.y*1.0);
  }

  public void m1(int x, double y) {
	  x = 1;
	  y = 1.0;
	  int q = x *20;
	  int[] r = new int[]{10,202};
	  q=m2(r);
	  float t = m3(y);
	  y = r[1]*1.0 + t;

	  A a = new A();
	  a.y = q;
	  z = a.y;
  }

  public int m2(int[] a) {
	  int z = 0;
	  z = (a[0]>0)?1:2;
	  a[1]=z;
	  return a[1];
  }

  public float m3(double b) {
	  float t;
	  if ( b > 10.5 ) t = 10.5f;
	  else t = 0.0f;
	  return t;
  }

}
