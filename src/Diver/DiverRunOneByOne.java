/**
 * File: src/Diver/DiverRunOneByOne.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 01/10/17     hcai        created; online algorithm based on the dynamic transfer graph rather than MDG
 * 01/11/17     hcai        completed and reached the first working version.
*/
package Diver;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
		
public class DiverRunOneByOne {
	static ImpactOneByOne icAgent = new ImpactOneByOne();
	
	public static void main(String args[]){
		if (args.length < 3) {
			System.err.println("Too few arguments: \n\t " +
					"DiverRunOneByOne subjectName subjectDir binPath \n\n");
			return;
		}
		String subjectName = args[0];

		String subjectDir = args[1]; 
		String binPath = args[2];
		
		Set<String> queries = null;
		
		File queryF = null;
		if (args.length > 3) {
			queryF = new File(args[3]);
		}
		else {
			queryF = new File(binPath + File.separator + "functionList.out");
		}
		
		if (queryF.exists()) {
			queries = new HashSet<String>();
			readQueries(queries, queryF.getAbsolutePath());
		}
		else {
			System.err.println("invalid query list.");
			return;
		}
		
		System.out.print("Subject: " + subjectName + " Dir=" + subjectDir + " binpath=" + binPath );
		if (queries != null) {
			System.out.println (queries.size() + " queries");
		}
		
		if (DiverAnalysis.init(binPath) != 0) {
			System.out.println("Unable to load the static value transfer graph, aborted now.");
			return;
		}
		
		icAgent.setSVTG(binPath+File.separator+"staticVtg.dat");
		if (0 != icAgent.initializeGraph(false)) {
			System.out.println("Unable to load the static value transfer graph, aborted now.");
			return;
		}
		
		try {
			if (queries!=null) {
				int qcnt = 0;
				for (String query : queries) {
					System.out.println("===== on query " + (++qcnt) + "/" + queries.size() + " =====");
					
					if (0 != icAgent.graphInit(query)) continue;
					EAMonitorOneByOne.setICAgent(icAgent);
					EAMonitorOneByOne.dvtgExercised = new ImpactOneByOne();
					
					startRunSubject(subjectName, subjectDir, binPath);
					
					icAgent.graphFinalize(EAMonitorOneByOne.dvtgExercised);
					Set<String> finalResult = EAMonitorOneByOne.dvtgExercised.getImpactSet(query);
					
					/*
					System.out.println("==== Diver impact set of [" + query +"]  size=" + finalResult.size() + " ====");
					for (String m : finalResult) {
						System.out.println(m);
					}
					*/
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void readQueries(Set<String> queryMethods, String fnQuerylist) {
		BufferedReader br;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(fnQuerylist)));
			String ts = br.readLine();
			while(ts != null) {
				queryMethods.add(ts.trim());
				ts = br.readLine();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void startRunSubject(final String name, String dir, final String binPath){
		int n = 0;
		BufferedReader br;
		   
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(dir+"/inputs/testinputs.txt")));
			String ts = br.readLine();
			while(ts != null){
				n++;
				System.out.println("current at the test No.  " + n);
				String [] args = preProcessArg(ts,dir);
				
				File runSub = new File(binPath);
				URL url = runSub.toURL();
			    URL[] urls = new URL[]{url};
			    
			    EAMonitorOneByOne.resetInternals();
			   
			    try {
			    	/*
					ClassLoader parentloader = new URLClassLoader(urls2);
				    ClassLoader cl = new URLClassLoader( urls, Thread.currentThread().getContextClassLoader() );				    
				    Thread.currentThread().setContextClassLoader(cl);
				    */
			    	ClassLoader cl = new URLClassLoader( urls, ClassLoader.getSystemClassLoader() );
				    Class cls = cl.loadClass(name);
				    
				    Method me=cls.getMethod("main", new Class[]{args.getClass()});
				    me.invoke(null, new Object[]{(Object)args});
				}
			    catch (InvocationTargetException e) {
			    	e.getTargetException().printStackTrace();
			    }
				catch (Exception e) {
					e.printStackTrace();
				}

			   // invoke the "program termination event" for the subject in case there is uncaught exception occurred
			    EAMonitorOneByOne.terminate("Enforced by DiverRunOneByOne.");

			   ts = br.readLine();
			}
			
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String[] preProcessArg(String arg,String dir){
		String s1 = arg.replaceAll("\\\\+","/").replaceAll("\\s+", " ");
 
		if(s1.startsWith(" "))
			s1 = s1.substring(1,s1.length());
		String argArray[] =  s1.split(" ");
		for(int i=0;i<argArray.length;i++){
			if(argArray[i].startsWith("..")){
				argArray[i] = argArray[i].replaceFirst("..", dir);
			}
		}		
		return argArray;
	}
}

/* vim :set ts=4 tw=4 tws=4 */
