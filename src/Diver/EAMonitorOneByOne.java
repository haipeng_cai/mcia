/**
 * File: src/Diver/EAMonitorOneByOne.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      	Changes
 * -------------------------------------------------------------------------------------------
 * 01/10/17     hcai            created; online algorithm based on dynamic transfer graph rather than MDG; thus serve one query in one run
 * 01/11/17     hcai            completed and reached the first working version.
*/
package Diver;

import java.io.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

public class EAMonitorOneByOne {
	protected static final long CN_LIMIT = 1*1000*1000;
	/* the global counter for time-stamping each method event */
	protected static Integer g_counter = 0;
	
	protected static ImpactOneByOne icAgent = null;
	protected static ImpactOneByOne dvtgExercised = null;
	public static void setICAgent(ImpactOneByOne _agent) {icAgent = _agent;}
	
	protected static int g_eventCnt = 0;
	
	/* a flag ensuring the initialization and termination are both executed exactly once and they are paired*/
	protected static boolean bInitialized = false;

	private static boolean active = false;
	
	/* buffering events */
	protected static List<Integer> B = new LinkedList<Integer>();
	
	/* clean up internal data structures that should be done so for separate dumping of them, a typical such occasion is doing this per test case */
	public synchronized static void resetInternals() {
		g_eventCnt = 0;
		g_counter = 0;
		B.clear();
		bInitialized = false;
		active = false;
	}
	
	/* initialize the two maps and the global counter upon the program start event */		
	public synchronized static void initialize() throws Exception{
		//resetInternals();
		bInitialized = true;
	}
	
	public synchronized static void enter(String methodname){
		if (0 == g_counter) {
			System.out.println("buffering events ......");
		}
		if (active) return;
		active = true;
		try {
			
			Integer smidx = icAgent.getMethodIdx(methodname);
			if (smidx==null || !icAgent.isMethodInSVTG(smidx)) {
				return;
			}
			
			B.add(smidx*-1);
			g_counter ++;
			g_eventCnt ++;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			active = false;
		}
	}

	/* the callee could be either an actual method called or a trap */
	public synchronized static void returnInto(String methodname, String calleeName){
		if (active) return;
		active = true;
		try {
			Integer smidx = icAgent.getMethodIdx(methodname);
			if (smidx==null || !icAgent.isMethodInSVTG(smidx)) {
				return;
			}
			
			B.add(smidx);
			g_counter ++;
			g_eventCnt ++;
			
			if (g_counter > CN_LIMIT) {
				processEvents();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			active = false;
		}
	}
	
	public synchronized static void terminate(String where) throws Exception {
		processEvents();
		if (bInitialized) {
			bInitialized = false;
		}
		else {
			return;
		}
	}
	
	protected synchronized static void processEvents() {
		System.out.println("\nstart processing events in the buffer of " + B.size() + " events...... ");
        if (null == dvtgExercised) return;
		for (Integer _idx : B) {
			//System.out.println("event no. " + g_eventCnt++);
			//System.out.print(".");
			Integer smidx = Math.abs(_idx);
			
			// enter event
			if (_idx < 0) {
				icAgent.graphUpdateOnEnterEvent(dvtgExercised, smidx, g_eventCnt);
			}
			// return-into event
			else {
				icAgent.graphUpdateOnReturnedIntoEvent(dvtgExercised, smidx, g_eventCnt);
			}
		}
		System.out.println();
		g_counter = 0;
		B.clear();
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

/* vim :set ts=4 tw=4 tws=4 */

