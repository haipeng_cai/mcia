/**
 * File: src/Diver/EAMonitorAllInOne.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      	Changes
 * -------------------------------------------------------------------------------------------
 * 12/31/15		hcai			created; compute all impacts during runtime, without producing traces
 * 01/03/16		hcai			added checks against deep-recursive entrance to the handlers due to recursions in the subject runtime
 * 01/05/16     hcai            added an option of computing impact set for specified queries only
 * 01/10/17     hcai            add in-memory buffer in the online event processing - process events by sets rather than one by one
*/
package Diver;

import java.io.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

public class EAMonitorAllInOne {
	protected static final long CN_LIMIT = 1*1000*1000;
	/* the global counter for time-stamping each method event */
	protected static Integer g_counter = 0;
	
	protected static ImpactAllInOne icAgent = null;
	public static void setICAgent(ImpactAllInOne _agent) {icAgent = _agent;}
	
	protected static Integer preMethod = null;
	
	protected static int g_eventCnt = 0;
	
	/* a flag ensuring the initialization and termination are both executed exactly once and they are paired*/
	protected static boolean bInitialized = false;

	private static boolean active = false;
	
	private static boolean start = false;
	
	/* buffering events */
	protected static List<Integer> B = new LinkedList<Integer>();
	
	/* clean up internal data structures that should be done so for separate dumping of them, a typical such occasion is doing this per test case */
	public synchronized static void resetInternals() {
		preMethod = null;
		g_eventCnt = 0;
		start = false;
		g_counter = 0;
		B.clear();
	}
	
	/* initialize the two maps and the global counter upon the program start event */		
	public synchronized static void initialize() throws Exception{
		//resetInternals();
		bInitialized = true;
	}
	
	public synchronized static void enter(String methodname){
		if (0 == g_counter) {
			System.out.println("buffering events ......");
		}
		if (active) return;
		active = true;
		try {
			
			Integer smidx = ImpactAllInOne.getMethodIdx(methodname);
			if (smidx==null || !ImpactAllInOne.isMethodInSVTG(smidx)) {
				return;
			}
			
			B.add(smidx*-1);
			g_counter ++;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			active = false;
		}
	}

	/* the callee could be either an actual method called or a trap */
	public synchronized static void returnInto(String methodname, String calleeName){
		if (active) return;
		active = true;
		try {
			Integer smidx = ImpactAllInOne.getMethodIdx(methodname);
			if (smidx==null || !ImpactAllInOne.isMethodInSVTG(smidx)) {
				return;
			}
			
			B.add(smidx);
			g_counter ++;
			
			if (g_counter > CN_LIMIT) {
				processEvents();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			active = false;
		}
	}
	
	public synchronized static void terminate(String where) throws Exception {
		if (bInitialized) {
			bInitialized = false;
		}
		else {
			return;
		}
		processEvents();
	}
	
	protected synchronized static void processEvents() {
		System.out.println("\nstart processing events in the buffer of " + B.size() + " events...... ");
		for (Integer _idx : B) {
			//System.out.println("event no. " + g_eventCnt++);
			//System.out.print(".");
			Integer smidx = Math.abs(_idx);
			
			if (null != preMethod && preMethod == smidx) {
				continue;
			}
			
			
			if (!start) {
				start = (ImpactAllInOne.getAllQueries()==null || ImpactAllInOne.getAllQueries().contains(smidx));
				if (!start) {
					continue;
				}
			}
			
			// enter event
			if (_idx < 0) {
				// trivially each method, once executed, is treated as impacted by itself
				if (!icAgent.getAllImpactSets().containsKey(smidx)) {
					icAgent.add2ImpactSet(smidx, smidx);
				}
				
				icAgent.onMethodEntryEvent(smidx);
			}
			// return-into event
			else {
				icAgent.onMethodReturnedIntoEvent(smidx);
			}
			
			if (null != preMethod && preMethod != smidx) {
				// close some "open" source nodes
				icAgent.closeNodes(preMethod, smidx);
			}
			
			preMethod = smidx;
		}
		System.out.println();
		g_counter = 0;
		B.clear();
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

/* vim :set ts=4 tw=4 tws=4 */

