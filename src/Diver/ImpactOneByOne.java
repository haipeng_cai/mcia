/**
 * File: src/Diver/ImpactOneByOne.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 01/11/17		hcai		created; online version of dynamic transfer graph; for online impact 
 *                          computation serving one query at a time
 *
*/
package Diver;

import java.io.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.zip.GZIPInputStream;

import MciaUtil.*;
import MciaUtil.VTEdge.*;

public class ImpactOneByOne extends ValueTransferGraph<DVTNode, DVTEdge> implements Serializable {
	private static final long serialVersionUID = 0x638200DE;
	/* the static VTG as the source of the initial dynamic VTG */
	transient protected final static StaticTransferGraph svtg = new StaticTransferGraph();
	/* the full sequence of EAS events */
	transient protected static LinkedHashMap<Integer, Integer> EASeq = null;
	/* the map from index to method signature associated with the full sequence of method events */
	transient protected static LinkedHashMap<Integer, String> EAMethodMap = null;
	
	/* the file holding the static VTG binary */
	transient protected static String fnSVTG = "";
	
	// a map from a method to all transfer edges on the static graph that are associated with the method
	transient protected /*static*/ Map<Integer, List<DVTNode>> method2nodes;
	// a map from an edge type to all transfer edges of the type of the static graph
	transient protected /*static*/ Map<VTEdge.VTEType, List<DVTEdge>> type2edges;
	/** map from a node to all incoming edges */
	transient protected /*static*/ Map< DVTNode, Set<DVTEdge> > nodeToInEdges;
	
	/** a map from method signature to index for the underlying static VTG */
	transient protected /*static*/ Map< String, Integer > method2idx;
	/** a map from index to method signature for the underlying static VTG */
	transient protected /*static*/ Map< Integer, String > idx2method;
	
	transient public Map<dua.util.Pair<Integer, String>, Map<Integer, Set<Integer>>> objIDMaps = null; 

	public ImpactOneByOne() {
		super();
	}
	
	protected void initInternals() {
		super.initInternals();
		//if (null==svtg) svtg = new StaticTransferGraph();
		/*if (null==method2nodes)*/ method2nodes = new LinkedHashMap<Integer, List<DVTNode>>();
		/*if (null==type2edges)*/ type2edges = new LinkedHashMap<VTEType, List<DVTEdge>>();
		/*if (null==nodeToInEdges)*/ nodeToInEdges = new LinkedHashMap< DVTNode, Set<DVTEdge> >();
		method2idx = new LinkedHashMap<String, Integer>();
		idx2method = new LinkedHashMap<Integer, String>();
	}
	
	public void setSVTG(String _fnSVTG) {
		fnSVTG = _fnSVTG;
	}
	public Set<DVTEdge> getInEdges(DVTNode _node) { 
		return nodeToInEdges.get(_node); 
	}
	
	public boolean isEmpty() {
		return super.isEmpty() || nodeToInEdges.isEmpty() || method2nodes.isEmpty() || type2edges.isEmpty();
	}
	public void clear() {
		super.clear();
		this.nodeToInEdges.clear();
		this.method2nodes.clear();
		this.type2edges.clear();
		
		this.method2idx.clear();
		this.idx2method.clear();
	}
	
	public void deepCopyFrom(ImpactOneByOne vtg) { 
		this.clear();
	
		for (DVTEdge _e : vtg.edges) {
			this.createTransferEdge(_e.getSource().getVar(), _e.getSource().getMethod(), _e.getSource().getStmt(),
					_e.getTarget().getVar(), _e.getTarget().getMethod(), _e.getTarget().getStmt(), _e.getEdgeType());
		}
		
		this.classifyEdgeAndNodes();
	}
	
	private void classifyEdgeAndNodes() {
		// 1. build the method->VTG nodes map to facilitate edge activation and source-target matching later on
		/* list nodes by enclosing methods */
		for (DVTNode vn : nodes) {
			List<DVTNode> vns = method2nodes.get(vn.getMethod());
			if (vns == null) {
				vns = new LinkedList<DVTNode>();
				method2nodes.put(vn.getMethod(), vns);
			}
			vns.add(vn);
		}
		// 2. build the EdgeType->VTG edges map 
		for (DVTEdge edge : edges) {
			List<DVTEdge> els = type2edges.get(edge.getEdgeType());
			if (els == null) {
				els = new LinkedList<DVTEdge>();
				type2edges.put(edge.getEdgeType(), els);
			}
			els.add(edge);
		}
	}
	
	public void CopyFrom(ImpactOneByOne vtg) {
		this.clear();
		super.CopyFrom(vtg);
		
		nodeToInEdges = vtg.nodeToInEdges;
		method2nodes = vtg.method2nodes;
		type2edges = vtg.type2edges;
		//this.classifyEdgeAndNodes();
	}
	
	@Override public int buildGraph(boolean debugOut) throws Exception{
		int index = 0;
		for (SVTNode sn : svtg.nodeSet()) {
			String mname = sn.getMethod().getName();
			if (!method2idx.containsKey(mname)) {
				method2idx.put(mname, index);
				idx2method.put(index, mname);
				index ++;
			}
		}
		for (SVTEdge se : svtg.edgeSet()) {
			createTransferEdge(
					utils.getCanonicalFieldName(se.getSource().getVar()),
					//utils.getFullMethodName(se.getSource().getMethod()),
					method2idx.get(se.getSource().getMethod().getName()),
					utils.getFlexibleStmtId(se.getSource().getStmt()),
					utils.getCanonicalFieldName(se.getTarget().getVar()),
					//utils.getFullMethodName(se.getTarget().getMethod()),
					method2idx.get(se.getTarget().getMethod().getName()),
					utils.getFlexibleStmtId(se.getTarget().getStmt()),
					se.getEdgeType());
		}
		return 0;
	}
	
	/**
	 * load the initializing static value transfer graph from a disk file previously dumped
	 * @param sfn the static graph file name
	 * @return 0 for success and others for failure
	 */
	public int initializeGraph(String sfn, boolean debugOut) {
		// 1. deserialize the static transfer graph firstly
		if ( null == svtg.DeserializeFromFile(sfn) ) {
			return -1;
		}
		
		// 2. initialize the dynamic graph with the literal representation of the underlying static graph
		try {
			buildGraph(false);
		}
		catch (Exception e) {
			return -1;
		}
		
		// 3. classify internal graph structures
		classifyEdgeAndNodes();
		
		if (debugOut) {
			System.out.println("original static graph: " + svtg);
			System.out.println("===== The Initial Dynamic VTG [loaded from the static counterpart] =====");
			dumpGraphInternals(true);
		}
	
		return 0;
	}
	public int initializeGraph(boolean debugOut) {
		if (null == fnSVTG || fnSVTG.length() < 1) {
			return -1;
		}
		return initializeGraph(fnSVTG, debugOut);
	}
		
	protected void createTransferEdge(DVTNode src, DVTNode tgt, VTEType etype) {
		DVTEdge edge = new DVTEdge(src, tgt, etype);
		if (!edges.contains(edge)) {
			addEdge(edge);
		}
	}
	
	/** avoid creating redundant instances of a same node */ 
	private DVTNode getCreateDVTNode(String var, Integer method, Integer stmt) {
		DVTNode ret = new DVTNode(var, method, stmt);
		if (nodes.contains(ret)) {
			for (DVTNode n : nodes) {
				if (n.equals(ret)) {
					return n;
				}
			}
			assert false;
		}
		return ret;
	}

	/** a convenient routine for adding an edge and the covered nodes into the graph */
	private void createTransferEdge(String srcVar, Integer srcMethod, Integer srcStmt,
						String tgtVar, Integer tgtMethod, Integer tgtStmt, VTEType etype) {
		// TODO:
		// any transfer edges associated with methods unreachable from entry should be ignored

		DVTNode src = /*new DVTNode*/getCreateDVTNode(srcVar, srcMethod, srcStmt);
		DVTNode tgt = /*new DVTNode*/getCreateDVTNode(tgtVar, tgtMethod, tgtStmt);
	
		createTransferEdge(src, tgt, etype);
	}
	
	public void addEdge(DVTEdge edge) {
		if (edges.contains(edge)) return;
		DVTNode src = edge.getSource(), tgt = edge.getTarget();
		
		nodes.add(src);
		nodes.add(tgt);
		edges.add(edge);
		
		Set<DVTEdge> outEdges = nodeToEdges.get(src);
		if (null == outEdges) {
			outEdges = new HashSet<DVTEdge>();
		}
		outEdges.add(edge);
		nodeToEdges.put(src, outEdges);
		
		Set<DVTEdge> inEdges = nodeToInEdges.get(tgt);
		if (null == inEdges) {
			inEdges = new HashSet<DVTEdge>();
		}
		inEdges.add(edge);
		nodeToInEdges.put(tgt, inEdges);
	}
	
	/**
	 * if user input gives method name only, or not matching any existing method name in upper/lower case,
	 * we first match "valid" names as an effective change set before computing the impact sets of them
	 * @param chg
	 * @return
	 */
	public Set<String> getChangeSet(String chg) {
		Set<String> chgSet = new LinkedHashSet<String>();
		for (Integer _m : method2nodes.keySet()) {
			String mn = idx2method.get(_m);
			if (mn.toLowerCase().contains(chg.toLowerCase())) {
				chgSet.add(mn);
			}
		}
		return chgSet;
	}
	
	private void getImpactSetImpl(DVTNode start, Set<DVTNode> visited, Set<String> mis) {
		if (!visited.add(start)) {
			return;
		}
		mis.add( idx2method.get(start.getMethod()) );
		if (null == getOutEdges(start)) return;
		
		// find the incoming edge with maximal time stamp at its starting point
		int mts = Integer.MIN_VALUE;
		for (DVTEdge _e : getOutEdges(start)) {
			// continue propagation
			getImpactSetImpl(_e.getTarget(), visited, mis);
		}
	}
	
	public Set<String> getImpactSet(String chgm) {
		Set<String> mis = new LinkedHashSet<String>();
		// trivially the change method itself is always in the impact set of it
		/** No! if it is never executed or statically never called, it should not be */
		//mis.add(chgm);
		
		Integer chgmIdx = method2idx.get(chgm);
		assert chgmIdx!=null;
		
		Set<DVTNode> visited = new LinkedHashSet<DVTNode>();
		List<DVTNode> startNodes = method2nodes.get(chgmIdx);
		if (startNodes != null) {
			for (DVTNode _n : startNodes) {
				getImpactSetImpl(_n, visited, mis);
			}
		}
				
		return mis;
	}
	
	public boolean isMethodInSVTG(Integer midx) {
		return method2nodes.get(midx)!=null;
	}
	public Integer getMethodIdx(String mename) {
		if (mename==null) return null;
		return method2idx.get(mename);
	}
	
	/** a helper function, initialize the set of affected nodes by the given chgm, for 
	 * public Set<String> buildGraph(String chgm, boolean debugOut)
	 * namely, collect all directly reachable methods from chgm
	 */ 
	private void initAffectedNodes(Map<VTEType, Set<DVTNode>> nodes, VTEType etype, Integer chgm) {
		for (DVTNode _n : method2nodes.get(chgm)) {
			if (this.getOutEdges(_n) != null) {
				for (DVTEdge _e : this.getOutEdges(_n)) {
					//if (_e.getEdgeType().equals(etype)) 
					{
						nodes.get(etype).add(_e.getSource()); // any statement-level changes can happen at among these sources
						nodes.get(etype).add(_e.getTarget());
					}
				}
			}
		}
	}
	/** a helper function, updating affected nodes according to the current executed method, for 
	 * public Set<String> buildGraph(String chgm, boolean debugOut)
	 * namely, keep those that are directly reachable from any nodes in current set
	 */
	private void updateAffectedNodes(Map<VTEType, Set<DVTNode>> nodes, VTEType etype, Integer curm) {
		Set<DVTNode> nset = nodes.get(etype);
		if (nset == null || nset.isEmpty()) {
			// nothing to update
			return;
		}
		Set<DVTNode> _nset = new LinkedHashSet<DVTNode>(); // the new affected set
		for (DVTNode _n : nset) {
			if (this.getOutEdges(_n) != null) {
				for (DVTEdge _e : this.getOutEdges(_n)) {
					/** changes can be propagated forwards via any type of edges since we consider a single edge away here*/
					if (/*_e.getEdgeType().equals(etype) &&*/ _e.getTarget().getMethod().equals(curm)) {
						_nset.add(_e.getTarget());
					}
				}
			}
		}
		nset.clear();
		nset.addAll(_nset);
	}
	
	/** for different types of edges, we have different strategies for edge activation and source-target matching, so we maintain
	 * a list of open source nodes for each edge type
	 */
	Map< VTEType, Set<DVTNode> > openNodes = new HashMap<VTEType, Set<DVTNode>>();
	/** also, since we already know the query, we could record a propagation "flag" for each of the type of edge:
	 * more precisely, for Type 1 (Local) edges, the flag is always False, and for Type 3 (Heap object edges) it is always True;
	 * for Type 2 (one-method away, including parameterEdge, returnEdge and RefParamEdge) edges, the flag should be updated
	 * during the traversal according to the consecutive order in regard of the edge source method and edge target method
	 */
	Map<VTEType, Boolean> propFlags = new HashMap<VTEType, Boolean>();
	/** And, nodes to which the change from the origin (the query) has been propagated so far are recorded for deciding if we should
	 * mark nodes open, for each of the AdjacentEdges
	 */
	Map<VTEType, Set<DVTNode>> affectedNodes = new HashMap<VTEType, Set<DVTNode>>();
	
	Integer preMethod = null;
	// mark the first enter event of the change method
	boolean bChgEnter = false;
	// the query
	Integer chgmIdx = null;
	
	public int graphInit(String chgm) {
		if (svtg.isEmpty() || this.isEmpty() || chgm==null) {
			// initializeGraph must be invoked and return success in the first place
			return -1;
		}
		
		chgmIdx = method2idx.get(chgm);
		if (chgmIdx == null) {
			//System.err.println("cannot find method index for " + chgm);
			return -1;
		}
		
		for (VTEType etype : VTEType.values() /*type2edges.keySet()*/ ) {
			openNodes.put(etype, new LinkedHashSet<DVTNode>());
			/** Heap objects propagate changes across any number of intermediate methods if there is any edge connecting through*/
			propFlags.put(etype, !VTEdge.isAdjacentType(etype));
			affectedNodes.put(etype, new LinkedHashSet<DVTNode>());
		}
		
		preMethod = null;
		bChgEnter = false;
		
		return 0;
	}
	
	
	/**
	 * update a dynamic transfer graph for a specific change that has been given upon an enter event
	 * @param dvtg the new dynamic transfer graph as the result of exercising the current dynamic graph with the current trace and chgm
	 * @param em the method being entered
	 * @return 0 for success otherwise failure
	 */
	public int graphUpdateOnEnterEvent(ImpactOneByOne dvtg, Integer em, Integer timestamp) {
		// 1. make sure the dynamic graph has been initialized successfully
		if (svtg.isEmpty() || this.isEmpty()) {
			// initializeGraph must be invoked and return success in the first place
			return -1;
		}
		
		if (!bChgEnter) {
			// em includes the class name and method name, together giving the origin of change
			bChgEnter = chgmIdx.equals(em);
			if (!bChgEnter) {
				// methods occurred before the first enter of the change method won't be impacted by the change mehtod 
				return 0;
			}
		}
		
							
		// check each of all nodes associated with the currently checked method
		if (em==null || method2nodes.get(em) == null) {
			//System.out.println("associated with no nodes: " + em);
			return -1;
		}
		
		// update the "propagation flags" per currently encountered method
		for (VTEType etype : VTEType.values()/*type2edges.keySet()*/ ) {
			if (VTEdge.isAdjacentType(etype)) {
				// start or stop propagating change impact via Adjacent edges, depending on it the chgm occurred					
				propFlags.put(etype, /*isEnterEvent &&*/ chgmIdx.equals(em));
			}
		}
		
		if (propFlags.get(VTEType.VTE_PARAM)) {
			initAffectedNodes(affectedNodes, VTEType.VTE_PARAM, chgmIdx);
		}
		else {
			updateAffectedNodes(affectedNodes, VTEType.VTE_PARAM, em);
		}
		
		if (propFlags.get(VTEType.VTE_RET)) {
			initAffectedNodes(affectedNodes, VTEType.VTE_RET, chgmIdx);
		}
		else {
			updateAffectedNodes(affectedNodes, VTEType.VTE_RET, em);
		}
		
		if (propFlags.get(VTEType.VTE_PARARET)) {
			initAffectedNodes(affectedNodes, VTEType.VTE_PARARET, chgmIdx);
		}
		else {
			updateAffectedNodes(affectedNodes, VTEType.VTE_PARARET, em);
		}
				
		for (DVTNode _n : method2nodes.get(em)) {
			// attach the event's time stamp to the dynamic VTG node
			_n.setTimestamp(timestamp);
			
			// examine each of all the outgoing edges from the node
			Set<DVTEdge> oedges = getOutEdges(_n);
			if (null != oedges) {
				for (DVTEdge _e : oedges) {
					// 1. local edges, all to be activated once the hosting method got executed
					if (_e.isLocalEdge()) {
						
						dvtg.addEdge(_e);
						continue;
					}
					
					// 3. for Intra CD edges, treat them the same way as Local edges
					if (_e.isIntraControlEdge()) {
						dvtg.addEdge(_e);
						continue;
					}
					
					// 2. for Heap edges, we mark the source node as "open"; for AdjacentEdges, it depends on its 
					// being in the affectedNodes or not
					if (!VTEdge.isAdjacentType(_e.getEdgeType()) || 
							affectedNodes.get(_e.getEdgeType()).contains(_e.getSource()) ) {
						openNodes.get(_e.getEdgeType()).add(_e.getSource());
						continue;
					}
					
				} // for each outgoing edge
			}
			
			// examine each of all the incoming edges towards the node
			Set<DVTEdge> iedges = getInEdges(_n);
			if (iedges != null) {
				for (DVTEdge _e : iedges) {
					// 1. local edges, all to be activated once the hosting method got executed
					if (_e.isLocalEdge()) {
						
						dvtg.addEdge(_e);
						continue;
					}
					
					// 2. Heap object edges and parameter edges are activated upon the matching of the target with "open" source
					if (_e.isParameterEdge() || _e.isHeapEdge()) {
						// match open source for the target
						if ( openNodes.get(_e.getEdgeType()).contains( _e.getSource() )) {
							
							dvtg.addEdge(_e);
						}
						continue;
					}
					
					// 3. for Intra CD edges, treat them the same way as Local edges
					if (_e.isIntraControlEdge()) {
						dvtg.addEdge(_e);
						continue;
					}
					
					// 4. for Inter CD edges, we treat them the way similar to Heap Edges
					if (_e.isInterControlEdge()) {
						// match open source for the target
						if ( openNodes.get(_e.getEdgeType()).contains( _e.getSource() )) {
							dvtg.addEdge(_e);
						}
					}
				} // for each incoming edge
			}
		} // for each associated node
		
		if (null == preMethod || preMethod.equals(em)) {
			// nothing to do with the first event
			preMethod = em;
			return 0;
		}
		// close some "open" source nodes
		for (Map.Entry< VTEType, Set<DVTNode> > _en : openNodes.entrySet()) {
			/** Interprocedural CD edges can pass across any number of methods, like Heap object transfer edges */
			if ( !VTEdge.isAdjacentType(_en.getKey()) /*&& !ValueTransferGraph.isControlType(_en.getKey()) */) {
				// close nodes for "adjacent type" edges only
				continue;
			}
			Set<DVTNode> toRemove = new HashSet<DVTNode>();
			for (DVTNode _n : _en.getValue()) {
				// close nodes marked open by the previous event
				if (_n.getMethod().equals(preMethod)) {
					//openNodes.get(_en.getKey()).remove(_n);
					toRemove.add(_n);
				}
			}
			openNodes.get(_en.getKey()).removeAll(toRemove);
		}
		
		preMethod = em;
		
		return 0;
	}

	/**
	 * update a dynamic transfer graph for a specific change that has been given upon a returned-into event
	 * @param dvtg the new dynamic transfer graph as the result of exercising the current dynamic graph with the current trace and chgm
	 * @param chgm the origin of change
	 * @param emstr the method being returned into
	 * @return 0 for success otherwise failure
	 */
	public int graphUpdateOnReturnedIntoEvent(ImpactOneByOne dvtg, Integer em, Integer timestamp) {
		// 1. make sure the dynamic graph has been initialized successfully
		if (svtg.isEmpty() || this.isEmpty()) {
			// initializeGraph must be invoked and return success in the first place
			return -1;
		}
		
		if (!bChgEnter) {
			// em includes the class name and method name, together giving the origin of change
			bChgEnter = chgmIdx.equals(em);
			if (!bChgEnter) {
				// methods occurred before the first enter of the change method won't be impacted by the change mehtod 
				return 0;
			}
		}
		
							
		// check each of all nodes associated with the currently checked method
		if (em==null || method2nodes.get(em) == null) {
			//System.out.println("associated with no nodes: " + em);
			return -1;
		}
		
		// update the "propagation flags" per currently encountered method
		for (VTEType etype : VTEType.values()/*type2edges.keySet()*/ ) {
			if (VTEdge.isAdjacentType(etype)) {
				// start or stop propagating change impact via Adjacent edges, depending on it the chgm occurred					
				propFlags.put(etype, /*isEnterEvent &&*/ chgmIdx.equals(em));
			}
		}
		
		if (propFlags.get(VTEType.VTE_PARAM)) {
			initAffectedNodes(affectedNodes, VTEType.VTE_PARAM, chgmIdx);
		}
		else {
			updateAffectedNodes(affectedNodes, VTEType.VTE_PARAM, em);
		}
		
		if (propFlags.get(VTEType.VTE_RET)) {
			initAffectedNodes(affectedNodes, VTEType.VTE_RET, chgmIdx);
		}
		else {
			updateAffectedNodes(affectedNodes, VTEType.VTE_RET, em);
		}
		
		if (propFlags.get(VTEType.VTE_PARARET)) {
			initAffectedNodes(affectedNodes, VTEType.VTE_PARARET, chgmIdx);
		}
		else {
			updateAffectedNodes(affectedNodes, VTEType.VTE_PARARET, em);
		}
				
		for (DVTNode _n : method2nodes.get(em)) {
			// attach the event's time stamp to the dynamic VTG node
			_n.setTimestamp(timestamp);
			
			
			/** upon ReturnedInto event, we need check the outgoing edges too, but 
			 * marking open nodes for adjacent edges only
			 */
			// examine each of all the outgoing edges from the node
			Set<DVTEdge> oedges = getOutEdges(_n);
			if (null != oedges) {
				for (DVTEdge _e : oedges) {
					// 1. local edges, all to be activated once the hosting method got executed
					// 2. for Adjacent edges, we mark the source node as "open"
					/** For outgoing heap object edges, we should have opened the source nodes upon the enter event of this method,
					 *  and, since those heap edges can transfer changes across any #methods away, they must not be "closed" once
					 *  opened in the occurrence of the enter event, which must happen before this returnInto event 
					 */
					if ( _e.isAdjacentEdge() && affectedNodes.get(_e.getEdgeType()).contains(_e.getSource()) ){
						openNodes.get(_e.getEdgeType()).add(_e.getSource());
						continue;
					}
					
					// 4. for Inter CD edges, we treat them the way similar to Adjacent Edges but would not worry about multi-hop
				}
			}
			
			// examine each of all the incoming edges towards the node
			Set<DVTEdge> iedges = getInEdges(_n);
			if (null != iedges) {
				for (DVTEdge _e : iedges) {
					// 1. Local edges: have already been added when processing the enter event of the relevant methods
					
					// 2. Return edges follow the same rule of matching as the RefParam edges since the latter can be essentially
					//     regarded as a kind of Return;
					// And, heap object edges can also transfer changes from callee to caller
					if (_e.isReturnEdge() || _e.isRefReturnParamEdge() || _e.isHeapEdge()) {
						if ( openNodes.get(_e.getEdgeType()).contains( _e.getSource() )) {
							
							dvtg.addEdge(_e);
						}
						continue;
					}
					
					// 3. Intra CD edges: have already been added when processing the enter event of the relevant methods
					
					// 4. Inter CD edges are added when the target gets paired ONLY upon the entrance of the target method
					// just like the Parameter edges
				}
			}
		} // for each associated node
				
		if (null == preMethod || preMethod.equals(em)) {
			// nothing to do with the first event
			preMethod = em;
			return 0;
		}
		// close some "open" source nodes
		for (Map.Entry< VTEType, Set<DVTNode> > _en : openNodes.entrySet()) {
			//if ( ! (_en.getKey().equals(VTEType.VTE_PARAM) || _en.getKey().equals(VTEType.VTE_PARARET) ||	_en.getKey().equals(VTEType.VTE_RET)) ) {
			/** Interprocedural CD edges can pass across any number of methods, like Heap object transfer edges */
			if ( !VTEdge.isAdjacentType(_en.getKey()) /*&& !ValueTransferGraph.isControlType(_en.getKey()) */) {
				// close nodes for "adjacent type" edges only
				continue;
			}
			Set<DVTNode> toRemove = new HashSet<DVTNode>();
			for (DVTNode _n : _en.getValue()) {
				// close nodes marked open by the previous event
				if (_n.getMethod().equals(preMethod)) {
					//openNodes.get(_en.getKey()).remove(_n);
					toRemove.add(_n);
				}
			}
			openNodes.get(_en.getKey()).removeAll(toRemove);
		}
		
		preMethod = em;
		
		return 0;
	}
	
	public void graphFinalize(ImpactOneByOne dvtg) {
		// now, update the graph to the really dynamic one
		dvtg.classifyEdgeAndNodes();
		// these two maps are the same between the static and dynamic VTG since when we are using the indices for
		// methods we refer to the same global index map, which is unique for each static VTG
		dvtg.method2idx = method2idx;
		dvtg.idx2method = idx2method;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///                                           SERIALIZATION AND DESERIALIZATION
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override public ImpactOneByOne DeserializeFromFile(String sfn) {
		Object o = super.DeserializeFromFile(sfn);
		if (o != null) {
			ImpactOneByOne vtg = new ImpactOneByOne();
			vtg = (ImpactOneByOne)o;
			//vtg.readObject(ois);
			this.CopyFrom(vtg);
			return vtg;
		}
			
		return null;
	} // DeserializeFromFile
	
	/**
	 * for debugging purposes, list all edge details
	 * @param listByEdgeType
	 */
	@Override public int dumpGraphInternals(boolean listByEdgeType) {
		if (0 == super.dumpGraphInternals(listByEdgeType)) {
			return 0;
		}
		
		System.out.println("---------- method indexing map -------------");
		for (Map.Entry<Integer, String> en : idx2method.entrySet()) {
			System.out.println(en.getKey() + " : " + en.getValue());
		}
		
		for (Map.Entry<Integer, List<DVTNode>> en : method2nodes.entrySet()) {
			System.out.println("----------------------------------------- " + 
					idx2method.get(en.getKey()) + " [" +	en.getValue().size() + 
					" nodes] -----------------------------------------");
			for (DVTNode vn : en.getValue()) {
				System.out.println("\t"+vn);
			}
		}
		
		/* list edges by types */
		for (Map.Entry<VTEType, List<DVTEdge>> en : type2edges.entrySet()) {
			System.out.println("----------------------------------------- " + 
					VTEdge.edgeTypeLiteral(en.getKey()) + " [" +	en.getValue().size() + 
					" edges] -----------------------------------------");
			for (DVTEdge edge : en.getValue()) {
				System.out.println("\t"+edge);
			}
		}
		return 0;
	}
} // definition of ImpactOneByOne

/* vim :set ts=4 tw=4 tws=4 */
