/**
 * File: src/MDG/DynMDGInst.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 12/31/14		hcai		created; for the static-analysis phase of DynMDG-based IA
 * 01/08/15		hcai		reached the working version
*/
package MDG;

import dua.Forensics;
import fault.StmtMapper;

import soot.*;
import soot.util.dot.DotGraph;

import EAS.*;

public class DynMDGInst extends EAInst {
	
	protected static MdgOptions opts = new MdgOptions();
	
	public static void main(String args[]){
		args = preProcessArgs(opts, args);

		DynMDGInst dvInst = new DynMDGInst();
		// examine catch blocks
		dua.Options.ignoreCatchBlocks = false;
		
		Forensics.registerExtension(dvInst);
		Forensics.main(args);
	}
	
	@Override protected void init() {
		clsMonitor = Scene.v().getSootClass("Diver.EAMonitor");
		mInitialize = clsMonitor.getMethodByName("initialize");
		mEnter = clsMonitor.getMethodByName("enter");
		mReturnInto = clsMonitor.getMethodByName("returnInto");
		mTerminate = clsMonitor.getMethodByName("terminate");
	}
	
	@Override public void run() {
		System.out.println("Running DynMDG extension of DUA-Forensics");
		// we would want to retrieve the Jimple statement ids for the VTG nodes
		StmtMapper.getCreateInverseMap();
		
		// 1. create the static value transfer graph
		int ret = createMDG();
		
		// 2. instrument EAS events
		if (ret==0) {
			instrument();
		}
	}
	
	private int createMDG() {
		MethodDepGraph mdg = new MethodDepGraph();
		try {
			final long startTime = System.currentTimeMillis();
			mdg.setIncludeIntraCD(opts.includeIntraCD());
			mdg.setIncludeInterCD(opts.includeInterCD());
			mdg.setExInterCD(opts.includeExceptionalInterCD());
			mdg.setIgnoreRTECD(opts.ignoreRTECD());
			
			mdg.buildGraph(opts.debugOut());
			final long stopTime = System.currentTimeMillis();
			System.out.println("MDG construction took " + (stopTime - startTime) + " ms");
		}
		catch (Exception e) {
			System.out.println("Error occurred during the construction of MDG");
			e.printStackTrace();
			return -1;
		}

		if (opts.debugOut()) {
			mdg.dumpGraphInternals(true);
		}
		else {
			System.out.println(mdg);
		}
			
		// DEBUG: test serialization and deserialization
		if (opts.includeIntraCD()) {
			String fn = dua.util.Util.getCreateBaseOutPath() + "staticmdg.dat";
			if ( 0 == mdg.SerializeToFile(fn) ) {
				//if (opts.debugOut()) 
				{
					System.out.println("======== MDG successfully serialized to " + fn + " ==========");
					MethodDepGraph g = new MethodDepGraph();
					if (null != g.DeserializeFromFile (fn)) {
						System.out.println("======== MDG loaded from disk file ==========");
						//g.dumpGraphInternals(true);
						System.out.println(g);
					}
				}
			} // test serialization/deserialization
		} // test static MDG construction
		
		// DEBUG: visualize the MDG for debugging and paper writing purposes
		if (opts.visualizeMDG()) {
			//String dotfn = soot.SourceLocator.v().getOutputDir() + java.io.File.separator + "mdg";
			String dotfn = dua.util.Util.getCreateBaseOutPath() + "mdg" + DotGraph.DOT_EXTENSION;
			mdg.visualizeVTG(dotfn);
		}
		
		return 0;
	} // -- createVTG
} // -- public class DynMDGInst  

/* vim :set ts=4 tw=4 tws=4 */

