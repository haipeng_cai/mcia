/**
 * File: src/MDG/MdgOptions.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 12/30/14		hcai		created; for command-line argument processing for MDG-based static IA
 * 01/08/15		hcai		reached the working version
*/
package MDG;

import java.util.ArrayList;
import java.util.List;

import Diver.DiverOptions;

public class MdgOptions extends DiverOptions {
	/* if visualizing MDG */
	protected boolean visualizeMDG = false;
	protected boolean queryAll = false;
	protected String query = "";
	
	public String query() { return query; }
	
	public boolean visualizeMDG() { return visualizeMDG; }
	public boolean includeIntraCD() { return intraCD; }
	public boolean includeInterCD() { return interCD; }
	public boolean includeExceptionalInterCD() { return exceptionalInterCD; }
	public boolean ignoreRTECD() { return ignoreRTECD; }
	public boolean queryAll() { return queryAll; }

	public final static int OPTION_NUM = DiverOptions.OPTION_NUM + 1;
	
	@Override public String[] process(String[] args) {
		args = super.process(args);
		
		List<String> argsFiltered = new ArrayList<String>();
		for (int i = 0; i < args.length; ++i) {
			String arg = args[i];

			if (arg.equals("-serializeMDG")) {
				visualizeMDG = true;
			}
			else if (arg.equals("-queryAll")) {
				// query the entire program (all methods)
				queryAll = true;
			}
			else if (arg.equals("-query")) {
				// give the changed methods (to be queried), separated by comma if there are more than one
				query = arg;
			}
			else {
				argsFiltered.add(arg);
			}
		}
		
		String[] arrArgsFilt = new String[argsFiltered.size()];
		return super.process( (String[]) argsFiltered.toArray(arrArgsFilt) );
	}
}

/* vim :set ts=4 tw=4 tws=4 */

