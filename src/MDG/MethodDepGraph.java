/**
 * File: src/Diver/MethodDepGraph.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 12/22/14		hcai		created; for implementing the method-level dependence graph (with summary edges to 
 *							abstract away intraprocedural dependence edges
 * 12/28/14		hcai		done the draft coding for MDG implementation
 * 01/07/15		hcai		fixed important bug in the MDG construction: summary edges are created but then all removed. 
*/
package MDG;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import dua.method.CFGDefUses.Variable;

import soot.*;
import soot.jimple.*;

import Diver.SVTEdge;
import Diver.SVTNode;
import Diver.StaticTransferGraph;
import MciaUtil.*;
import MciaUtil.VTEdge.VTEType;

public class MethodDepGraph extends StaticTransferGraph implements Serializable {
	/** map from a node to all incoming edges */
	transient protected Map< SVTNode, Set<SVTEdge> > nodeToInEdges;
	// a map from a method to all nodes that are associated with the method
	transient protected /*static*/ Map<SootMethod, List<SVTNode>> method2nodes;
	// a map from an edge type to all transfer edges of the type of the static graph
	transient protected /*static*/ Map<VTEdge.VTEType, List<SVTEdge>> type2edges;
		
	@Override protected void initInternals() {
		super.initInternals();
		nodeToInEdges = new HashMap< SVTNode, Set<SVTEdge> >();
		method2nodes = new HashMap<SootMethod, List<SVTNode>>();
		type2edges = new HashMap<VTEType, List<SVTEdge>>();
	}
	public MethodDepGraph() {
		super();
	}
	
	@Override public String toString() {
		return "[Static MDG] derived from " + super.toString();
	}
	
	public Set<SVTEdge> getInEdges(SVTNode _node) { 
		return nodeToInEdges.get(_node); 
	}
	public boolean isEmpty() {
		return super.isEmpty() || nodeToInEdges.isEmpty()
			|| method2nodes.isEmpty() || type2edges.isEmpty();
	}
	public void clear() {
		super.clear();
		this.nodeToInEdges.clear();
		this.method2nodes.clear();
		this.type2edges.clear();
	}
	
	protected void fillNodeToInEdge() {
		// build the node->incoming edges mapping to facilitate node-edge relationship querying
		for (SVTEdge e : this.edges) {
			SVTNode tgt = e.getTarget();
			Set<SVTEdge> inEdges = nodeToInEdges.get(tgt);
			if (null == inEdges) {
				inEdges = new HashSet<SVTEdge>();
			}
			inEdges.add(e);
			nodeToInEdges.put(tgt, inEdges);
		}
	}
	
	/** build the MDG based on the statement level dependence graph (VTG)
	 * @param debugOut true for logging internal steps and notices during the graph construction
	 * @return 0 for success, otherwise, exception thrown tells error messages
	 */ 
	public int buildGraph(boolean debugOut) {
		try {
			super.buildGraph(debugOut);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		fillNodeToInEdge();
		
		/** testing: fake MDG, actually still the statement-level dependence graph */
		/*
		if (!debugOut) {
			classifyEdgeAndNodes();
			return 0;
		}
		*/
		
		// now that the statement-level dependence graph has been created; simply do now the summarization 
		// of intraprocedural edges
		// 1. collect incoming and outgoing ports for each method
		Map<SootMethod, Set<SVTNode>> mToIPs = new HashMap<SootMethod, Set<SVTNode>>();
		Map<SootMethod, Set<SVTNode>> mToOPs = new HashMap<SootMethod, Set<SVTNode>>();
		for (SVTEdge e : this.edges) {
			//if (!e.isLocalEdge() && !e.isIntraControlEdge()) {
			if (e.isInterproceduralEdge()) {
				SootMethod mi = e.getTarget().getMethod();
				SootMethod mo = e.getSource().getMethod();
				
				assert mi != mo;
				
				// gather IPs for the target method
				Set<SVTNode> ni = mToIPs.get(mi);
				if (ni == null) {
					ni = new HashSet<SVTNode>();
					mToIPs.put(mi, ni);
				}
				ni.add(e.getTarget());
				
				// gather OPs for the source method
				Set<SVTNode> no = mToOPs.get(mo);
				if (no == null) {
					no = new HashSet<SVTNode>();
					mToOPs.put(mo, no);
				}
				no.add(e.getSource());
			}
		}
		// 2. for each method, create summary edges connecting IPs to OPs of that method
		Set<SVTEdge> sumedges = new HashSet<SVTEdge>();
		for (SootMethod m : mToIPs.keySet()) {
			if (!mToOPs.containsKey(m)) {
				System.err.println("Warning: method " + m + " has IPs but no OPs, skipped!");
				continue;
			}
			
			// match each IP against each OP checking if there is a path between them via intraprocedural edges
			for (SVTNode in : mToIPs.get(m)) {
				for (SVTNode on : mToOPs.get(m)) {
					boolean isReachable = isIntraReachable(in, on, new HashSet<SVTNode>());
					if (isReachable) {
						// to add a summary edge directly connecting 'in' to 'on'
						sumedges.add(new SVTEdge(in, on, VTEdge.VTEType.VTE_INTRA));
					}
				}
			}
		}
		
		// remove all original intraprocedural edges inside each method
		Set<SVTEdge> e2del = new HashSet<SVTEdge>();
		for (SVTEdge e : this.edges) {
			//if (e.isLocalEdge() || e.isIntraControlEdge()) {
			if (!e.isInterproceduralEdge()) {
				e2del.add(e);
			}
		}
		for (SVTEdge e:e2del) {
			removeEdge(e);
		}
		// clean up orphan nodes (no incoming nor outgoing edges connected to/from them)
		Set<SVTNode> n2del = new HashSet<SVTNode>();
		for (SVTNode n : this.nodes) {
			if ( (!nodeToEdges.containsKey(n) || nodeToEdges.get(n).size()<1) && 
					(!nodeToInEdges.containsKey(n) || nodeToInEdges.get(n).size()<1) ) {
				n2del.add(n);
			}
		}
		for (SVTNode n:n2del) {
			removeNode(n);
		}
		
		// add all summary edges finally
		for (SVTEdge se : sumedges) {
			this.addEdge(se);
		}
		
		classifyEdgeAndNodes();
		return 0;
	}
	
	private void removeEdge(SVTEdge e) {
		edges.remove(e);
		nodeToEdges.get(e.getSource()).remove(e);
		nodeToInEdges.get(e.getTarget()).remove(e);
	}
	private void removeNode(SVTNode n) {
		nodes.remove(n);
		nodeToEdges.remove(n);
		nodeToInEdges.remove(n);
	}
	
	private boolean isIntraReachable(SVTNode a, SVTNode b, Set<SVTNode> visited) {
		if (a==null || b==null) return false;
		if (!visited.add(a)) {
			return false;
		}
		Set<SVTEdge> oe = this.getOutEdges(a);
		if (oe == null || oe.size()<1) return false;
		
		for (SVTEdge e : oe) {
			//if (e.isLocalEdge() || e.isIntraControlEdge()) {
			if (!e.isInterproceduralEdge()) {	
				if (e.getTarget().equals(b)) {
					return true;
				}
				if (isIntraReachable(e.getTarget(), b, visited)) {
					return true;
				}
			}
		}
		
		return false;
	}
	private void classifyEdgeAndNodes() {
		// 1. build the method->VTG nodes map to facilitate edge activation and source-target matching later on
		/* list nodes by enclosing methods */
		for (SVTNode vn : nodes) {
			List<SVTNode> vns = method2nodes.get(vn.getMethod());
			if (vns == null) {
				vns = new LinkedList<SVTNode>();
				method2nodes.put(vn.getMethod(), vns);
			}
			vns.add(vn);
		}
		// 2. build the EdgeType->VTG edges map 
		for (SVTEdge edge : edges) {
			List<SVTEdge> els = type2edges.get(edge.getEdgeType());
			if (els == null) {
				els = new LinkedList<SVTEdge>();
				type2edges.put(edge.getEdgeType(), els);
			}
			els.add(edge);
		}
	}
	public void deepCopyFrom(MethodDepGraph mdg) { 
		this.clear();
	
		for (SVTEdge _e : mdg.edges) {
			this.createTransferEdge(_e.getSource().getVar(), _e.getSource().getMethod(), _e.getSource().getStmt(),
					_e.getTarget().getVar(), _e.getTarget().getMethod(), _e.getTarget().getStmt(), _e.getEdgeType());
		}
		
		this.classifyEdgeAndNodes();
	}
	protected void createTransferEdge(SVTNode src, SVTNode tgt, VTEType etype) {
		SVTEdge edge = new SVTEdge(src, tgt, etype);
		if (!edges.contains(edge)) {
			addEdge(edge);
		}
	}
	
	/** avoid creating redundant instances of a same node */ 
	private SVTNode getCreateSVTNode(Variable var, SootMethod method, Stmt stmt) {
		SVTNode ret = new SVTNode(var, method, stmt);
		if (nodes.contains(ret)) {
			for (SVTNode n : nodes) {
				if (n.equals(ret)) {
					return n;
				}
			}
			assert false;
		}
		return ret;
	}

	/** a convenient routine for adding an edge and the covered nodes into the graph */
	private void createTransferEdge(Variable srcVar, SootMethod srcMethod, Stmt srcStmt,
						Variable tgtVar, SootMethod tgtMethod, Stmt tgtStmt, VTEType etype) {
		SVTNode src = getCreateSVTNode(srcVar, srcMethod, srcStmt);
		SVTNode tgt = getCreateSVTNode(tgtVar, tgtMethod, tgtStmt);
	
		createTransferEdge(src, tgt, etype);
	}
	
	public void addEdge(SVTEdge edge) {
		if (edges.contains(edge)) return;
		SVTNode src = edge.getSource(), tgt = edge.getTarget();
		
		nodes.add(src);
		nodes.add(tgt);
		edges.add(edge);
		
		Set<SVTEdge> outEdges = nodeToEdges.get(src);
		if (null == outEdges) {
			outEdges = new HashSet<SVTEdge>();
		}
		outEdges.add(edge);
		nodeToEdges.put(src, outEdges);
		
		Set<SVTEdge> inEdges = nodeToInEdges.get(tgt);
		if (null == inEdges) {
			inEdges = new HashSet<SVTEdge>();
		}
		inEdges.add(edge);
		nodeToInEdges.put(tgt, inEdges);
	}
	
	public Set<String> getChangeSet(String chg) {
		Set<String> chgSet = new LinkedHashSet<String>();
		for (SootMethod _m : method2nodes.keySet()) {
			if (_m.getSignature().toLowerCase().contains(chg.toLowerCase())) {
				chgSet.add(_m.getSignature());
			}
		}
		return chgSet;
	}
	
	private void getImpactSetImpl(SVTNode start, Set<SVTNode> visited, Set<String> mis) {
		if (!visited.add(start)) {
			return;
		}
		mis.add( start.getMethod().getSignature() );
		if (null == getOutEdges(start)) return;
		
		// find the incoming edge with maximal time stamp at its starting point
		for (SVTEdge _e : getOutEdges(start)) {
			// continue propagation
			getImpactSetImpl(_e.getTarget(), visited, mis);
		}
	}
	
	public Set<String> getImpactSet(SootMethod chgm) {
		Set<String> mis = new LinkedHashSet<String>();
		
		Set<SVTNode> visited = new LinkedHashSet<SVTNode>();
		List<SVTNode> startNodes = method2nodes.get(chgm);
		if (startNodes != null) {
			for (SVTNode _n : startNodes) {
				getImpactSetImpl(_n, visited, mis);
			}
		}
				
		return mis;
	}
	
	public SootMethod getMethodByName(String m) {
		for (SootMethod _m : method2nodes.keySet()) {
			if (_m.getSignature().toLowerCase().contains(m.toLowerCase())) {
				return _m;
			}
		}
		
		return null;
	}
	
	public Set<String> getImpactSet(String chgm) {
		Set<String> mis = new LinkedHashSet<String>();
		//if (!chgm.getActiveBody().getUnits().isEmpty()) 
		{
			mis.add(chgm);
		}
		mis.addAll(getImpactSet(getMethodByName(chgm)));

		return mis;
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///                                           SERIALIZATION AND DESERIALIZATION
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private static final long serialVersionUID = 0x488200FE;
    
    private void writeObject(java.io.ObjectOutputStream s)   throws IOException {
        s.defaultWriteObject();
        s.flush();
    }
     
    private void readObject(java.io.ObjectInputStream s) throws IOException, ClassNotFoundException {
        s.defaultReadObject();
    }

  	@Override public MethodDepGraph DeserializeFromFile(String sfn) {
  		Object o = super.DeserializeFromFile(sfn);
		if (o != null) {
			MethodDepGraph mdg = new MethodDepGraph();
			mdg = (MethodDepGraph)o;
			fillNodeToInEdge();
	        classifyEdgeAndNodes();
			return mdg;
		}
		return null;
	} // DeserializeFromFile
  	
  	public void CopyFrom(MethodDepGraph mdg) {
		this.clear();
		
		this.nodes = mdg.nodes;
		this.edges = mdg.edges;
		this.nodeToEdges = mdg.nodeToEdges;
		this.heflowsens = mdg.heflowsens;
		this.nodeToInEdges = mdg.nodeToInEdges;
		
		this.classifyEdgeAndNodes();
	}
  	
	@Override public int dumpGraphInternals(boolean listByEdgeType) {
		if (0 == super.dumpGraphInternals(listByEdgeType)) {
			return 0;
		}
		
		for (Map.Entry<SootMethod, List<SVTNode>> en : method2nodes.entrySet()) {
			System.out.println("----------------------------------------- " + 
					en.getKey() + " [" +	en.getValue().size() + 
					" nodes] -----------------------------------------");
			for (SVTNode vn : en.getValue()) {
				System.out.println("\t"+vn);
			}
		}
		
		/* list edges by types */
		for (Map.Entry<VTEType, List<SVTEdge>> en : type2edges.entrySet()) {
			System.out.println("----------------------------------------- " + 
					VTEdge.edgeTypeLiteral(en.getKey()) + " [" +	en.getValue().size() + 
					" edges] -----------------------------------------");
			for (SVTEdge edge : en.getValue()) {
				System.out.println("\t"+edge);
			}
		}
		return 0;
	}
} // definition of MethodDepGraph

/* vim :set ts=4 tw=4 tws=4 */
