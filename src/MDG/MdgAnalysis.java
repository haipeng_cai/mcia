/**
 * File: src/MDG/MdgAnalysis.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 12/30/14		hcai		created; for querying impact sets from the MDG static dependence model
 * 01/08/15		hcai		reached the working version
*/
package MDG;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import MciaUtil.utils;

import dua.Extension;
import dua.Forensics;
import fault.StmtMapper;

import soot.util.dot.DotGraph;

public class MdgAnalysis implements Extension {
	
	protected static MdgOptions opts = new MdgOptions();
	
	// the underlying static method-level dependence model
	protected final static MethodDepGraph mdg = new MethodDepGraph();
	
	static Set<String> changeSet = new LinkedHashSet<String>();
	static Map<String, Set<String> > impactSets = new LinkedHashMap<String, Set<String>>();
	 
	public static void main(String args[]){
		args = preProcessArgs(opts, args);

		MdgAnalysis sea = new MdgAnalysis();
		// examine catch blocks
		dua.Options.ignoreCatchBlocks = false;
		//dua.Options.skipDUAAnalysis = true;
		
		Forensics.registerExtension(sea);
		Forensics.main(args);
	}
	
	protected static String[] preProcessArgs(MdgOptions _opts, String[] args) {
		opts = _opts;
		args = opts.process(args);
		
		String[] argsForDuaF;
		int offset = 0;

		argsForDuaF = new String[args.length + 2 - offset];
		System.arraycopy(args, offset, argsForDuaF, 0, args.length-offset);
		argsForDuaF[args.length+1 - offset] = "-paramdefuses";
		argsForDuaF[args.length+0 - offset] = "-keeprepbrs";
		
		return argsForDuaF;
	}
	
	@Override public void run() {
		System.out.println("Running MDG extension of DUA-Forensics");
		StmtMapper.getCreateInverseMap();
	
		// 1. create the static MDG
		int ret = createMDG();
		
		// 2. query 
		if (ret != 0) { return; }
		if (!opts.query().isEmpty()) {
			// queryAll();
			startProcessQueries(opts.query());
			return;
		}
		if (!opts.queryAll()) {
			return;
		}
		
		// or process all possible queries
		final Set<String> allAppFuncs = new HashSet<String>();
		utils.getFunctionList(allAppFuncs);
		Map<String, Set<String>> allResults = new HashMap<String, Set<String>>();
		Map<String, Long> allTimecosts = new HashMap<String, Long>();
		for (String m : allAppFuncs) {
			// testing
			if (opts.debugOut()) {
				System.out.println("Impact set of " + m + " is following:\n\t " + querySingle(m));
			}
			long st = System.currentTimeMillis();
			allResults.put(m, querySingle(m));
			allTimecosts.put(m, System.currentTimeMillis() - st);
		}
		// serialize the results
		String sfn = dua.util.Util.getCreateBaseOutPath() + "mdgResults.dat";
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(sfn);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(allResults);
			oos.writeObject(allTimecosts);
			oos.flush();
			oos.close();
			System.out.println("All MDG query results have been serialized to " + sfn);
		}
		catch (Exception e) {
			System.err.println("Error occurred during the serialization of all MDG query results.");
			e.printStackTrace();
		}
	}
	
	private int createMDG() {
		try {
			final long startTime = System.currentTimeMillis();
			mdg.setIncludeIntraCD(opts.includeIntraCD());
			mdg.setIncludeInterCD(opts.includeInterCD());
			mdg.setExInterCD(opts.includeExceptionalInterCD());
			mdg.setIgnoreRTECD(opts.ignoreRTECD());
			// for heap edges, we need to apply flow sensitivity for this static IA approach
			// mdg.setHeFlowSens(true);
			
			mdg.buildGraph(opts.debugOut());
			final long stopTime = System.currentTimeMillis();
			System.out.println("MDG construction took " + (stopTime - startTime) + " ms");
		}
		catch (Exception e) {
			System.out.println("Error occurred during the construction of MDG");
			e.printStackTrace();
			return -1;
		}

		if (opts.debugOut()) {
			mdg.dumpGraphInternals(true);
		}
		else {
			System.out.println(mdg);
		}
			
		// DEBUG: visualize the MDG for debugging and paper writing purposes
		if (opts.visualizeMDG()) {
			//String dotfn = soot.SourceLocator.v().getOutputDir() + java.io.File.separator + "mdg";
			String dotfn = dua.util.Util.getCreateBaseOutPath() + "mdg" + DotGraph.DOT_EXTENSION;
			mdg.visualizeVTG(dotfn);
		}
		return 0;
	} // -- createMDG
	
	/** find all methods possibly to be impacted by f */
	public static Set<String> querySingle(String f) {
		return mdg.getImpactSet(f);
	}
	
	public static Set<String> queryAll() {
		Set<String> S = new HashSet<String>();
		
		List<String> Chglist = dua.util.Util.parseStringList(opts.query(), ';');
		if (Chglist.size() < 1) {
			// nothing to do
			System.err.println("Empty query, nothing to do.");
			return S;
		}
		
		for (String f : Chglist) {
			S.addAll(querySingle(f));
		}
		return S;
	}
	
	public static int obtainValidChangeSet(String changedMethods) {
		changeSet.clear();  // in case this method gets multiple invocations from external callers 
		List<String> Chglist = dua.util.Util.parseStringList(changedMethods, ';');
		if (Chglist.size() < 1) {
			// nothing to do
			System.err.println("Empty query, nothing to do.");
			return -1;
		}
		// determine the valid change set
		Set<String> validChgSet = new LinkedHashSet<String>();
		for (String chg : Chglist) {
			validChgSet.addAll(mdg.getChangeSet(chg));
		}
		if (validChgSet.isEmpty()) {
			// nothing to do
			// System.out.println("Invalid queries, nothing to do.");
			return 0;
		}
		changeSet.addAll(validChgSet);
		return changeSet.size();
	}
	public static Set<String> getChangeSet() {
		return changeSet;
	}
	
	private static void printStatistics (Map<String, Set<String>> mis, boolean btitle) {
		if (btitle) {
			System.out.println("\n============ Diver Result ================");
			System.out.println("[Valid Change Set]");
			for (String m:changeSet) {
				System.out.println(m);
			}
		}
		Set<String> aggregatedIS = new LinkedHashSet<String>();
		for (String m : mis.keySet()) {
			System.out.println("[Change Impact Set of " + m + "]: size= " + mis.get(m).size());
			for (String im : mis.get(m)) {
				System.out.println(im);
			}
			// merge impact sets of all change queries
			aggregatedIS.addAll(mis.get(m));
		}
		if (btitle) {
			System.out.println("\n[Change Impact Set of All Changes]: size= " + aggregatedIS.size());
			for (String im : aggregatedIS) {
				System.out.println(im);
			}
		}
	}
	
	public static void startProcessQueries(String changedMethods) {
		impactSets.clear();
		
		int nret = obtainValidChangeSet(changedMethods);
		if ( nret <= 0 ) {
			// nothing to do
			if (nret == 0) {
				// always output report so that post-processing script can work with the Diver result in a consistent way as if there were
				// some non-empty results
				printStatistics(impactSets, true);
			}
			return;
		}
		
		for (String chg : changeSet) {
			Set<String> singleImpactSet = querySingle(chg);
			
			if (impactSets.get(chg) == null) {
				impactSets.put(chg, new LinkedHashSet<String>());
			}
			impactSets.get(chg).addAll(singleImpactSet);
		}
		
		printStatistics(impactSets, true);
	}
} // -- public class MdgAnalysis  

/* vim :set ts=4 tw=4 tws=4 */

