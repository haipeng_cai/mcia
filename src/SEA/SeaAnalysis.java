/**
 * File: src/SEA/SeaAnalysis.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 12/17/14		hcai		created; for querying static impact sets based on Sea
 * 12/19/14		hcai		finished the draft; to test/debug the functionality pursuant to SEA
 * 12/22/14		hcai		debugged with schedule1, nanoXML and XMLsecurity
 * 12/30/14		hcai		unified impact-set querying interface to be consistent with Diver & PI/EAS for reusing experimental scripts
 * 01/09/15		hcai		reached the working version (worked for all the four legacy subjects)
 * 01/14/15		hcai		spent a lot of time trying to use the Soot-offered JimpleBasedInterproceduralCFG for an icfg construction;
 * 							involved many troubles in library dependencies of this project (esp. Heros and Guava, etc.); yet this icfg 
 * 							does not seem to work: always get runtime exception when trying to traverse it; gave up continuing to try
 * 01/15/15		hcai		final desperate try with SEA implementation: many more examinations and even do transitive closure directly 
 * 							on the icfg, still cann't get 100% recall (actually relatively low recall) against static slice -> gave up!
 * 01/16/15		hcai		the recall problem finally got cleared out with the patient explanation from the SEA authors: it is the 
 * 							static slicer problem rather than that of the basic concept that "any data flows among statements must be 
 * 							realized through some control flows"; the static slices were so imprecise, because I forgot to set the 
 * 							'reachability' option, that it falsely shows that the SEA (and MDG) results are not of 100% recall.  
*/
package SEA;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import MciaUtil.CompleteUnitGraphEx;
import MciaUtil.InterCFGraphEx;
import MciaUtil.utils;
import dua.Extension;
import dua.Forensics;
import dua.util.Pair;

import soot.SootMethod;
import soot.Unit;
import soot.jimple.Stmt;
import soot.util.dot.DotGraph;
//import soot.jimple.toolkits.ide.icfg.*;

public class SeaAnalysis implements Extension {
	
	protected static SeaOptions opts = new SeaOptions();
	
	protected final static InterCFGraphEx icfg = new InterCFGraphEx(opts.debugOut);
	protected final static InterComponentCFG iccfg = new InterComponentCFG(opts.debugOut);
	
	// collect all methods in order to filter out library functions in the resulting impact sets
	protected final static Set<SootMethod> allAppFuncs = new HashSet<SootMethod>();
	protected final static Set<String> allAppMethodNames = new HashSet<String>();
	
	static Set<String> changeSet = new LinkedHashSet<String>();
	static Map<String, Set<String> > impactSets = new LinkedHashMap<String, Set<String>>();
	 
	public static void main(String args[]){
		args = preProcessArgs(opts, args);

		SeaAnalysis sea = new SeaAnalysis();
		// examine catch blocks
		dua.Options.ignoreCatchBlocks = false;
		//dua.Options.skipDUAAnalysis = true;
		
		Forensics.registerExtension(sea);
		Forensics.main(args);
	}
	
	protected static String[] preProcessArgs(SeaOptions _opts, String[] args) {
		opts = _opts;
		args = opts.process(args);
		
		String[] argsForDuaF;
		int offset = 0;

		argsForDuaF = new String[args.length + 2 - offset];
		System.arraycopy(args, offset, argsForDuaF, 0, args.length-offset);
		argsForDuaF[args.length+1 - offset] = "-paramdefuses";
		argsForDuaF[args.length+0 - offset] = "-keeprepbrs";
		
		return argsForDuaF;
	}
	
	@Override public void run() {
		System.out.println("Running SEA extension of DUA-Forensics");
		//StmtMapper.getCreateInverseMap();
		utils.getAllMethods(allAppFuncs);
		utils.getFunctionList(allAppMethodNames);
		
		// remove artificially added functions for field access adaptations
		Set<SootMethod> toremove = new HashSet<SootMethod>();
		for (SootMethod f : allAppFuncs) {
			if (f.getName().contains("__hcai_get_") || f.getName().contains("__hcai_set_")) {
				toremove.add(f);
				allAppMethodNames.remove(f.getSignature());
			}
		}
		allAppFuncs.removeAll(toremove);
		
		//JimpleBasedInterproceduralCFG sooticfg = new JimpleBasedInterproceduralCFG();
		
		// 1. create the static ICCFG (Proposed by SEA)
		int ret = createICCFG();
		
		// 2. process specified queries
		if (ret != 0) { return; }
		if (!opts.query().isEmpty()) {
			// queryAll();
			startProcessQueries(opts.query());
			return;
		}
		
		if (!opts.queryAll()) {
			return;
		}
		
		// or process all possible queries
		Map<String, Set<String>> allResults = new HashMap<String, Set<String>>();
		Map<String, Long> allTimecosts = new HashMap<String, Long>();
		for (SootMethod m : allAppFuncs) {
			// testing
			if (opts.debugOut()) {
				if (opts.queryOnICFG()) {
					System.out.println("Impact set of " + m + " is following:\n\t " + querySingleicfg(m));
				}
				else {
					System.out.println("Impact set of " + m + " is following:\n\t " + querySingle(m));
				}
			}
			
			long st = System.currentTimeMillis();
			if (opts.queryOnICFG()) {
				allResults.put(m.getSignature(), querySingleicfg(m));
			}
			else {
				allResults.put(m.getSignature(), querySingle(m));
			}
			allTimecosts.put(m.getSignature(), System.currentTimeMillis() - st);
			/*
			Queue<Unit> que = new LinkedList<Unit>();
			Set<Unit> visited = new LinkedHashSet<Unit>();
			System.out.println("\tHead nodes [size=" + sooticfg.getStartPointsOf(m).size()+"]:");
			for(Unit h: sooticfg.getStartPointsOf(m)) {
				System.out.println("\t\t"+ h);
				que.add(h);
			}
			
			while (!que.isEmpty()) {
				Unit n = que.poll();
				visited.add(n);

				assert n!=null;
				if (sooticfg.getSuccsOf(n) == null) continue;
				System.out.println("\t" + n + " has " + sooticfg.getSuccsOf(n).size()+" descendants:");
				for (Unit u : sooticfg.getSuccsOf(n)) {
					System.out.println("\t\t"+ u + "");
					if (!visited.contains(u) && !que.contains(u))
					{
						//visited.add(n);
						que.add(u);
					}
				}
			}

			System.out.println("\tTail nodes [size=" + sooticfg.getEndPointsOf(m).size()+"]:");
			for(Unit t: sooticfg.getEndPointsOf(m)) {
				System.out.println("\t\t"+ t);
			}
			*/
		}
		// serialize the results
		String sfn = dua.util.Util.getCreateBaseOutPath() + "seaResults.dat";
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(sfn);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(allResults);
			oos.writeObject(allTimecosts);
			oos.flush();
			oos.close();
			System.out.println("All SEA query results have been serialized to " + sfn);
		}
		catch (Exception e) {
			System.err.println("Error occurred during the serialization of all SEA query results.");
			e.printStackTrace();
		}
	}
	
	private int createICCFG() {
		try {
			final long startTime = System.currentTimeMillis();
			if (opts.queryOnICFG()) {
				icfg.buildGraph();
			}
			else {
				iccfg.buildGraph();
			}
			final long stopTime = System.currentTimeMillis();
			if (opts.queryOnICFG()) {
				System.out.println("ICFG construction took " + (stopTime - startTime) + " ms");
			}
			else {
				System.out.println("ICCFG construction took " + (stopTime - startTime) + " ms");
			}
		}
		catch (Exception e) {
			System.out.println("Error occurred during the construction of ICCFG");
			e.printStackTrace();
			return -1;
		}

		if (opts.debugOut()) {
			if (opts.queryOnICFG()) {
				icfg.dumpGraph();
			}
			else {
				iccfg.dumpGraph();
			}
		}
		else {
			if (opts.queryOnICFG()) {
				System.out.println(icfg);
			}
			else {
				System.out.println(iccfg);
			}
		}
			
		// DEBUG: visualize the ICCFG for debugging and paper writing purposes
		if (opts.visualize()) {
			//String dotfn = soot.SourceLocator.v().getOutputDir() + java.io.File.separator + "iccfg";
			if (opts.queryOnICFG()) {
				String dotfn = dua.util.Util.getCreateBaseOutPath() + "icfg" + DotGraph.DOT_EXTENSION;
				icfg.visualizeGraph(dotfn);
			}
			else {
				String dotfn = dua.util.Util.getCreateBaseOutPath() + "iccfg" + DotGraph.DOT_EXTENSION;
				iccfg.visualizeGraph(dotfn);
			}
		}
		return 0;
	} // -- createICCFG
	
	/** find all methods that the query could return into : namely find all f s.t. RET(query, f) */ 
	private static void recurse(Unit start, Set<Unit> greyNodes, Set<Pair<Unit,Unit>> visitedEdges, Set<String> S) {
		if (null == start) return;
		List<Unit> preds = 	iccfg.getPredsOf(start);
		
		if (CompleteUnitGraphEx.isEntry((Stmt)start)) {
			String mname = iccfg.getMethodFromEntry(start).getSignature();
			assert mname != null;
			S.add(mname);
		}
		
		if (preds==null) { return; }
		//if (preds.size() < 1) return;
		
		for (Unit u : preds) {
			Pair<Unit, Unit> edge = new Pair<Unit, Unit>(start, u);
			
			// no edge should be visited more than once during the backward traversal
			if (visitedEdges.contains(edge)) continue;
			else visitedEdges.add(edge);
			
			if (CompleteUnitGraphEx.isEntry((Stmt)start)) {
				//greyNodes.add(u);
				List<Unit> succs = iccfg.getSuccsOf(u);
				if (null != succs) {
					for (Unit succ : succs) {
						if (!CompleteUnitGraphEx.isEntry((Stmt)succ)) {
							greyNodes.add(succ);
						}
					}
				}
			}
			
			recurse (u, greyNodes, visitedEdges, S);
		}
	}
	/** find all methods having SEA relation with f as the impacted method of f */
	public static Set<String> querySingle(SootMethod f) {
		Set<String> S = new HashSet<String>();
		Unit start = iccfg.getStart(f);
		if (null == start) {
			// invalid query, nothing to do
			return S;
		}
		
		Set<Unit> greyNodes = new HashSet<Unit>();
		Set<Pair<Unit, Unit>> visitedEdges = new HashSet<Pair<Unit, Unit>>();
		
		for (Unit c : iccfg.getComponents(f)) {
			greyNodes.add(c);
		}
		
		//if (!iccfg.getMethodByName(f).getActiveBody().getUnits().isEmpty()) 
		{
			// dismiss methods of empty body
			S.add(f.getSignature());
		}
		
		// 1. collect methods that f could return into
		recurse (start, greyNodes, visitedEdges, S);
		
		// sanity check only
		for (Unit n : greyNodes) {
			assert !CompleteUnitGraphEx.isEntry((Stmt)n);
		}
		
		// 2. collect methods that f calls or follow f in sequential control flow (transitively for both)
		Set<Unit> blackNodes = new HashSet<Unit>();
		while (!greyNodes.isEmpty()) {
			Unit u = greyNodes.iterator().next();
			assert !CompleteUnitGraphEx.isEntry((Stmt)u);
			// now, u is a component node
			greyNodes.remove(u);
			if (!blackNodes.contains(u)) {
				blackNodes.add(u);
			}
			
			if (null == iccfg.getSuccsOf(u)) { continue; }
			
			for (Unit succ : iccfg.getSuccsOf(u)) {
				if (!CompleteUnitGraphEx.isEntry((Stmt)succ)) {
					// for successor components of component u
					if (!greyNodes.contains(succ)  && !blackNodes.contains(succ)) {
						greyNodes.add(succ);	
					}
				}
			}
			
			for (Unit succ : iccfg.getSuccsOf(u)) {
				SootMethod m = null;
				if (CompleteUnitGraphEx.isEntry((Stmt)succ)) {
					// for procedures called by component u
					m = iccfg.getMethodFromEntry(succ);
					/*
				}
				else {
					Stmt ss = (Stmt)succ;
					assert ss.containsInvokeExpr();
					m = ss.getInvokeExpr().getMethod();
				}
					*/
					String mname = m.getSignature();
					assert mname != null;
					S.add(mname);
					
					for (Unit c : iccfg.getComponents(m)) {
						if (!greyNodes.contains(c) && !blackNodes.contains(c)) {
							greyNodes.add(c);	
						}
					}
				}
			}
		}
		
		S.retainAll(allAppMethodNames);
		return S;
	}
	
	public static int obtainValidChangeSet(String changedMethods) {
		changeSet.clear();  // in case this method gets multiple invocations from external callers 
		List<String> Chglist = dua.util.Util.parseStringList(changedMethods, ';');
		if (Chglist.size() < 1) {
			// nothing to do
			System.err.println("Empty query, nothing to do.");
			return -1;
		}
		// determine the valid change set
		Set<String> validChgSet = new LinkedHashSet<String>();
		for (String chg : Chglist) {
			validChgSet.add(chg);
		}
		if (validChgSet.isEmpty()) {
			// nothing to do
			// System.out.println("Invalid queries, nothing to do.");
			return 0;
		}
		changeSet.addAll(validChgSet);
		return changeSet.size();
	}
	public static Set<String> getChangeSet() {
		return changeSet;
	}
	
	public static Set<String> queryAll() {
		Set<String> S = new HashSet<String>();
		
		List<String> Chglist = dua.util.Util.parseStringList(opts.query(), ';');
		if (Chglist.size() < 1) {
			// nothing to do
			System.err.println("Empty query, nothing to do.");
			return S;
		}
		
		for (String f : Chglist) {
			if (opts.queryOnICFG()) {
				SootMethod m = icfg.getMethodByName(f);
				if (m != null) {
					S.addAll(querySingleicfg(m));
				}
			}
			else {
				SootMethod m = iccfg.getMethodByName(f);
				if (m != null) {
					S.addAll(querySingle(m));
				}
			}
		}
		return S;
	}
	
	private static void printStatistics (Map<String, Set<String>> mis, boolean btitle) {
		if (btitle) {
			System.out.println("\n============ SEA Result ================");
			System.out.println("[Valid Change Set]");
			for (String m:changeSet) {
				System.out.println(m);
			}
		}
		Set<String> aggregatedIS = new LinkedHashSet<String>();
		for (String m : mis.keySet()) {
			System.out.println("[Change Impact Set of " + m + "]: size= " + mis.get(m).size());
			for (String im : mis.get(m)) {
				System.out.println(im);
			}
			// merge impact sets of all change queries
			aggregatedIS.addAll(mis.get(m));
		}
		if (btitle) {
			System.out.println("\n[Change Impact Set of All Changes]: size= " + aggregatedIS.size());
			for (String im : aggregatedIS) {
				System.out.println(im);
			}
		}
	}
	
	public static void startProcessQueries(String changedMethods) {
		impactSets.clear();
		
		int nret = obtainValidChangeSet(changedMethods);
		if ( nret <= 0 ) {
			// nothing to do
			if (nret == 0) {
				// always output report so that post-processing script can work with the Diver result in a consistent way as if there were
				// some non-empty results
				printStatistics(impactSets, true);
			}
			return;
		}
		
		for (String chg : changeSet) {
			Set<String> singleImpactSet = null;
			if (opts.queryOnICFG()) {
				SootMethod m = icfg.getMethodByName(chg);
				if (m == null) {
					continue;
				}
				singleImpactSet = querySingleicfg(m);
			}
			else {
				SootMethod m = iccfg.getMethodByName(chg);
				if (m == null) {
					continue;
				}
				singleImpactSet = querySingle(m);
			}
			
			if (impactSets.get(chg) == null) {
				impactSets.put(chg, new LinkedHashSet<String>());
			}
			impactSets.get(chg).addAll(singleImpactSet);
		}
		
		printStatistics(impactSets, true);
	}
	
	private static void querySingleicfgImpl(Unit start, Set<Unit> visited, Set<String> S) {
		if (!visited.add(start)) return;
		List<Unit> succs = icfg.getSuccsOf(start);
		if (null == succs) return;
		for (Unit succ : succs) {
			Stmt ss = (Stmt) succ;
			if (ss.containsInvokeExpr()) {
				S.add(ss.getInvokeExpr().getMethod().getSignature());
			}
			querySingleicfgImpl(succ, visited, S);
		}
	}
	public static Set<String> querySingleicfg(SootMethod f) {
		Set<String> S = new HashSet<String>();
		Unit start = icfg.getStart(f);
		if (null == start) return S;
		
		Set<Unit> visitedNodes = new HashSet<Unit>();
		S.add(f.getSignature());
		querySingleicfgImpl(start, visitedNodes, S);
		
		S.retainAll(allAppMethodNames);
		return S;
	}
} // -- public class SeaAnalysis  

/* vim :set ts=4 tw=4 tws=4 */

