/**
 * File: src/SEA/SeaMdgComp.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 01/05/15		hcai		created; for comparing SEA based static IA results against MDG based
 * 01/23/15		hcai		changed to compare results on data points that are in the ground truth only (result feeding the 
 * 							the second argument is regarded as the ground truth)
*/
package SEA;

import java.io.*;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class SeaMdgComp {
	// the necessary location arguments that give the query results files */
	protected static String fnSEARes = "";
	protected static String fnMDGRes = "";
	
	/** keep the default output stream */
	private final static PrintStream stdout = System.out;
	private final static PrintStream stderr = System.err;
	
	static Map<String, Set<String>> allResultsMDG = new HashMap<String, Set<String>>();
	static Map<String, Long> allTimecostsMDG = new HashMap<String, Long>();
	static Map<String, Set<String>> allResultsSEA = new HashMap<String, Set<String>>();
	static Map<String, Long> allTimecostsSEA = new HashMap<String, Long>();
	
	static final boolean showAccuracy = true;
	
	/** the map from category to impact set: 
	 * 0: SEA (technique results)
	 * 1: MDG  (static slice ground-truth)
	 */
	protected static Map<Integer, Set<String>> finalResult = new LinkedHashMap<Integer, Set<String>>();
	
	public static void parseArgs(String args[]){
		assert args.length >= 2;
		fnSEARes = args[0];
		fnMDGRes = args[1];
		System.err.println("SEA results from: " + fnSEARes + "; MDG results from: " + fnMDGRes);
	}
	
	@SuppressWarnings("unchecked")
	public static void main(String args[]){
		if (args.length < 2) {
			System.err.println("too few arguments.");
			return;
		}
		parseArgs(args);
		
		if (fnSEARes.length()>=1) {
			File queryF = new File(fnSEARes);
			if (queryF.exists()) {
				FileInputStream fis;
				try {
					fis = new FileInputStream(fnSEARes);
					ObjectInputStream ois = new ObjectInputStream(fis);
					allResultsSEA = (Map<String, Set<String>>) ois.readObject();
					allTimecostsSEA = (Map<String, Long>) ois.readObject();
					ois.close();
					fis.close();
					System.out.println("All SEA query results have been deserialized from " + fnSEARes);
				}
				catch (Exception e) {
					System.err.println("Error occurred during the deserialization of all SEA query results.");
					e.printStackTrace();
				}
			}
			else {
				System.err.println("SEA query result file not found.");
				return;
			}
		}
		else {
			System.err.println("invalid SEA query result file.");
			return;
		}
		if (fnMDGRes.length()>=1) {
			File queryF = new File(fnMDGRes);
			if (queryF.exists()) {
				FileInputStream fis;
				try {
					fis = new FileInputStream(fnMDGRes);
					ObjectInputStream ois = new ObjectInputStream(fis);
					allResultsMDG = (Map<String, Set<String>>) ois.readObject();
					allTimecostsMDG = (Map<String, Long>) ois.readObject();
					ois.close();
					fis.close();
					System.out.println("All MDG query results have been deserialized from " + fnMDGRes);
				}
				catch (Exception e) {
					System.err.println("Error occurred during the deserialization of all MDG query results.");
					e.printStackTrace();
				}
			}
			else {
				System.err.println("MDG query result file not found.");
				return;
			}
		}
		else {
			System.err.println("invalid MDG query result file.");
			return;
		}
		
		for (String m : allResultsMDG.keySet()) {
			for (Integer cat = 0; cat < 2; cat++) {
				finalResult.put(cat, new LinkedHashSet<String>());
			}
			finalResult.get(1).addAll(allResultsMDG.get(m));
			
			assert allTimecostsMDG.containsKey(m);
			assert allResultsSEA.containsKey(m);
			assert allTimecostsSEA.containsKey(m);
			
			finalResult.get(0).addAll(allResultsSEA.get(m));
			
			Set<String> queries = new HashSet<String>();
			queries.add(m);
			dumpStatistics(queries, stdout,stderr, allTimecostsSEA.get(m), allTimecostsMDG.get(m));
		}
	}
	
	/** dump impact sets of MDG versus mDMDG for the specific mutation point in the given query method */ 
	public static void dumpStatistics(Set<String> queries, PrintStream os, PrintStream or, long SEATime, long MDGTime) {
		if (finalResult.get(0).size() < 1) {
			// if this query method is not covered by any of the nTests tests, nothing to be reported
			return;
		}
		
		String allqueries = "";
		int istr = 0;
		for (String queryMethod : queries) {
			istr ++;
			if (istr > 1) allqueries += "\n";
			allqueries += queryMethod;
		}
		
		// dump complete data
		or.println("==== SEA impact set of [" + allqueries +"]  size=" + finalResult.get(0).size() + " ===");
		for (String m : finalResult.get(0)) {
			or.println(m);
		}
		
		or.println("==== MDG impact set of [" + allqueries +"]  size=" + finalResult.get(1).size() + " ====");
		for (String m : finalResult.get(1)) {
			or.println(m);
		}
		
		// compute FPs and FNs (take SEAImpactSet as the ground truth: FP=MDGIS-SEAIS, FN=SEAIS-MDGIS)
		Set<String> IntersectionAll = new LinkedHashSet<String>(finalResult.get(0)); 
		IntersectionAll.retainAll(finalResult.get(1));
		Set<String> FPAll = new LinkedHashSet<String>(finalResult.get(0));
		FPAll.removeAll(finalResult.get(1));
		Set<String> FNAll = new LinkedHashSet<String>(finalResult.get(1));
		FNAll.removeAll(finalResult.get(0));
		
		or.println("==== SEAIS - MDGIS  size=" + FPAll.size() + " ====");
		for (String m : FPAll) {
			or.println(m);
		}
		or.println("==== MDGIS - SEAIS  size=" + FNAll.size() + " ====");
		for (String m : FNAll) {
			or.println(m);
		}
		or.println();
		or.flush();
		
		DecimalFormat df = new DecimalFormat("#.####");
		
		// compute statistics and dump
		String title = "\t" + queries + "\t";
		
		if (showAccuracy) {
			double prec = IntersectionAll.size()*1.0 / finalResult.get(0).size();
			double rec = IntersectionAll.size()*1.0 / finalResult.get(1).size();
			double EDTimeRatio = MDGTime<1?1.0:SEATime*1.0 / MDGTime ;
			
			os.print(title);
			os.print(finalResult.get(0).size() + "\t" + finalResult.get(1).size() + "\t" + FPAll.size() + "\t" +
					FNAll.size() + "\t" + df.format(prec) + "\t" + df.format(rec) + "\t" + df.format(FMeasure(prec,rec,1)) + "\t" + 
					SEATime + "\t" + MDGTime + "\t" +	df.format(EDTimeRatio) + "\n");
		}
		else {
			double DESizeRatio = finalResult.get(0).size()<1?1.0:finalResult.get(1).size()*1.0 / finalResult.get(0).size();
			double EDTimeRatio = SEATime<1?1.0:MDGTime*1.0 / SEATime;
			
			os.print(title);
			os.print(finalResult.get(0).size() + "\t" + finalResult.get(1).size() + "\t" + FPAll.size() + "\t" +
					FNAll.size() + "\t" + df.format(DESizeRatio) + "\t" + SEATime + "\t" + MDGTime + "\t" + 
					df.format(EDTimeRatio) + "\n");
		}
		
		os.println();
		os.flush();
	}
	public static double FMeasure(double p, double c, int b) {
		return ( (1+b*b)*(p*c)/(p*(b*b)+c) );
	}
	
} // SeaMdgComp

/* vim :set ts=4 tw=4 tws=4 */
