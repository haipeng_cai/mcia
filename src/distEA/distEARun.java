/**
 * File: src/distEA/distdistEARun.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 02/05/15		hcai		created; for running the distEA-instrumented subject
 * 02/12/15		hcai		added option for running a specific test only 
 * 02/17/15		hcai		force dumping trace upon abnormal external termination by adding a thread hook
*/
package distEA;
import java.io.*;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.Permission;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class distEARun {
	protected static class ExitException extends SecurityException 
    {
        public final int status;
        public ExitException(int status) 
        {
            super("There is no escape!");
            this.status = status;
        }
    }

    private static class NoExitSecurityManager extends SecurityManager 
    {
        @Override
        public void checkPermission(Permission perm) 
        {
            // allow anything.
        }
        @Override
        public void checkPermission(Permission perm, Object context) 
        {
            // allow anything.
        }
        @Override
        public void checkExit(int status) 
        {
            super.checkExit(status);
            if (tn == -1) {
            	throw new ExitException(status);
            }
            else { 
            	try {
					if (!perThread) distMonitor.terminate("Enforced by distEARun.");
					else distThreadMonitor.terminate("Enforced by distEARun.");
				} catch (Exception e) {
					e.printStackTrace();
				}
            }
        }
    }
	
	static String outputrootDir = "";
	static int tn = -1; // the particular test to run; -1 indicates running all tests in the given list (inputs/testinputs.txt)
	
	/** map from test to corresponding expected full EAS trace length */
	protected static Map<String, Integer> test2tracelen = null;
	
	/** to avoid getting stuck on some test execution we limit the maximal length of time for each, in minutes */
	protected static long MAXTESTDURATION = 3L;
	
	/** keep the default output stream */
	private final static PrintStream stdout = System.out;
	private final static PrintStream stderr = System.err;
	
	private static boolean perThread = false;
	
	public static void main(String args[]){
		if (args.length < 3) {
			System.err.println("Too few arguments: \n\t " +
					"distEARun subjectName subjectDir binPath [outputDir] \n");
			return;
		}
		
		/** add hook to catch SIGKILL/SIGTERM */
		Runtime.getRuntime().addShutdownHook( new Thread()
        {
          public void run()
          {
            // System.out.println( "Shutdown signal caught!" ) ;
        	/** guarantee that the trace, if any collected, gets dumped */
        	{
        		System.out.println("\n[Forced by distEARun upon external termination]: "+
        				"dumping method event trace of current process execution ...");
        	}
    		try {
				distMonitor.terminate("Forced upon external termination");
			} catch (Exception e) {
				e.printStackTrace();
			}
          }
        } ) ;
		
		String subjectName = args[0];

		String subjectDir = args[1]; 
		String binPath = args[2];
		
		if (args.length > 3) {
			outputrootDir = args[3];
		}
		
		if (args.length > 4) {
			// run a specific test case instead of all of them within the same JVM process
			tn = Integer.valueOf(args[4]);
			test2tracelen = new LinkedHashMap<String, Integer>();
		}
		
		if (args.length > 5) {
			perThread = args[5].compareToIgnoreCase("-perthread")==0;
		}
		
		if (args.length > 6) {
			if (args[6].compareToIgnoreCase("-nointercept")==0)
				if (!perThread) distMonitor.disable(); else distThreadMonitor.disable();
			else 
				if (!perThread) distMonitor.enable(); else distThreadMonitor.enable();
		}
				
		if (args.length > 7) {
			boolean monitorDebug = args[7].compareToIgnoreCase("-debug")==0;
			if (!perThread) distMonitor.turnDebugOut(monitorDebug); else distThreadMonitor.turnDebugOut(monitorDebug);
		}

		System.out.println("Subject: " + subjectName + " Dir=" + subjectDir + " binpath=" + binPath + " tn=" + tn);
		
		try {
			startRunSubject(subjectName, subjectDir, binPath);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (tn == -1) {
			dumpTracelengths(outputrootDir + "/traceLengths.txt");
		}
	}
	
	public static void startRunSubject(String name, String dir, String binPath){
		
		int n = 0;
		BufferedReader br;
		PrintStream stdout = System.out;
		String outputDir;
		if(outputrootDir.equals("")){
			outputDir = dir + "/";
		}else{
			outputDir = outputrootDir;
		}
		
		File dirF = new File(outputDir);
		if(!dirF.isDirectory())	dirF.mkdirs();
	
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(dir+"/inputs/testinputs.txt")));
			String ts = br.readLine();
			while(ts != null){
				n++;
				
				if (tn != -1 && tn != n) {
					ts = br.readLine();
					// run the specified test only
					continue;
				}

				final String [] args = preProcessArg(ts,dir);
				
				System.setOut(stdout);
				System.out.println("current at the test No.  " + n);
					
				// set the name of file as the serialization target of method event maps (F followed by L)
				File dirLoc = new File(outputDir  + "/test"+n);
				if(!dirLoc.isDirectory()) dirLoc.mkdirs();
				//distMonitor.setEventMapSerializeFile(outputDir  + "/test"+n+ ".em");
				if (!perThread) 
					distMonitor.setEventMapSerializeFile(dirLoc.getAbsolutePath() + File.separator + "trace.em");
				else 
					distThreadMonitor.setEventMapSerializeFile(dirLoc.getAbsolutePath() + File.separator + "trace.em");
				
				//String outputF = outputDir  + "/test"+n+ ".out";
				//String errF = outputDir  + "/test"+n+ ".err";
				String outputF = dirLoc.getAbsolutePath() + File.separator + "run.out";
				String errF = dirLoc.getAbsolutePath() + File.separator + "run.err";
				
				final File outputFile = new File(outputF);
				PrintStream out = new PrintStream(new FileOutputStream(outputFile)); 
				System.setOut(out); 
				
				final File errFile = new File(errF);
				PrintStream err = new PrintStream(new FileOutputStream(errFile)); 
				System.setErr(err);
				
				File runSub = new File(binPath);
				URL url = runSub.toURL();
			    final URL[] urls = new URL[]{url};
			    final String clsname = name;
			    // final String wdir = binPath;
			   
			    //if (tn == -1) {
			    // don't need take care of 'system.exit' invocation in the program if running a single test only
			    /** but still to enforce the termination event to be invoked */
			    System.setSecurityManager(new NoExitSecurityManager());
			    //}
				
				ExecutorService EAservice = Executors.newSingleThreadExecutor();
				try {
					Runnable EARunner = new Runnable() {
						@Override public void run() {
							try {
							    //ClassLoader clEA = new URLClassLoader( urls, Thread.currentThread().getContextClassLoader() );
							    //Thread.currentThread().setContextClassLoader(clEA);
								
								//Runtime.getRuntime().removeShutdownHook(Thread.currentThread());

							    ClassLoader cl = new URLClassLoader( urls, ClassLoader.getSystemClassLoader() );
							    Class<?> cls = cl.loadClass(clsname);
							    
							    Method me=cls.getMethod("main", new Class[]{args.getClass()});
							    me.invoke(null, new Object[]{(Object)args});
							    
								
								/*
							    List<String> command = new ArrayList<String>();
							    command.add("java");
							    command.add(clsname);
							    for (int i = 0; i < args.length; i++) {
							    	command.add(args[i]);
							    }
							    System.out.println("command to run : " + command);
							    ProcessBuilder pb = new ProcessBuilder(command);
							    */
								
								/*
								String separator = System.getProperty("file.separator");
								String classpath = System.getProperty("java.class.path");
								String path = System.getProperty("java.home") + separator + "bin" + separator + "java";
								ProcessBuilder pb = new ProcessBuilder(path, "-cp", classpath, clsname); //clazz.getCanonicalName());
							    
							    pb.redirectOutput(outputFile);
							    pb.redirectError(errFile);
							    pb.inheritIO();
							    Process proc = pb.start();
							    // pb.redirectErrorStream(true);
							    pb.directory(new File(wdir));
							    
							    proc.waitFor();
							    proc.exitValue();
							    */
							}
							catch (ExitException e) 
						    {
						      	System.out.println("Caught system.exit call, exit status="+ e.status);
						    }
							catch (Exception e) {
								e.printStackTrace();
							}
							
						}
					};
					Future<?>  EAfuture = EAservice.submit(EARunner);
					EAfuture.get(MAXTESTDURATION*60, TimeUnit.SECONDS);
				}
			    catch (final InterruptedException e) {
			        // The thread was interrupted during sleep, wait or join
			    	System.setErr(stderr);
					System.err.println("Running distEA at the test No.  " + n + " thread interrupted.");
					EAS.Monitor.terminate("Enforced by distEARun.");
					ts = br.readLine();
					EAservice.shutdown();
					//if (tn == -1) {
					// don't need take care of 'system.exit' invocation in the program if running a single test only
					System.setSecurityManager(null);
					//}
					continue;
			    }
			    catch (final TimeoutException e) {
			        // Took too long!
			    	System.setErr(stderr);
					System.err.println("Running distEA at the test No.  " + n + " TimeOut after " + MAXTESTDURATION*60 + " seconds");
					EAS.Monitor.terminate("Enforced by distEARun.");
					ts = br.readLine();
					EAservice.shutdown();
					System.setSecurityManager(null);
					continue;
			    }
			    catch (final ExecutionException e) {
			        // An exception from within the Runnable task
			    	System.setErr(stderr);
					System.err.println("Running distEA at the test No.  " + n + " exception thrown during test execution " + e);
					EAS.Monitor.terminate("Enforced by distEARun.");
					//System.exit(0);
					ts = br.readLine();
					EAservice.shutdown();
					System.setSecurityManager(null);
					continue;
			    }
			    finally {
			        EAservice.shutdown();
					System.setSecurityManager(null);
			    } 				

			   // invoke the "program termination event" for the subject in case there is uncaught exception occurred
			   if (!perThread) 
				   distMonitor.terminate("Enforced by distEARun.");
			   else 
				   distThreadMonitor.terminate("Enforced by distEARun.");
			   out.flush();
			   out.close();
			   err.close();
			   
			   // System.gc();
			   
			   /** save full trace length */
			   if (tn == -1) {
				   if (!perThread) 
					   test2tracelen.put("test"+n, distMonitor.getFullTraceLength());
				   else
					   test2tracelen.put("test"+n, distThreadMonitor.getFullTraceLength());
			   }
			   
			   ts = br.readLine();
			}
			
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	public static void dumpTracelengths(String fn) {
		File fObj = new File(fn);
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(fObj));
			
			for (Map.Entry<String, Integer> en : test2tracelen.entrySet()) {
				writer.write(en.getKey() + "\t" + en.getValue() +"\n");
			}
			writer.flush();
			writer.close();
		}
		catch (FileNotFoundException e) { System.err.println("Couldn't write file: " + fObj + e); }
		catch (SecurityException e) { System.err.println("Couldn't write file: " + fObj + e); }
		catch (IOException e) { System.err.println("Couldn't write file: " + fObj + e); }
	}

	public static String[] preProcessArg(String arg,String dir){
		String s1 = arg.replaceAll("\\\\+","/").replaceAll("\\s+", " ");
 
		if(s1.startsWith(" "))
			s1 = s1.substring(1,s1.length());
		String argArray[] =  s1.split(" ");
		for(int i=0;i<argArray.length;i++){
			if(argArray[i].startsWith("..")){
				argArray[i] = argArray[i].replaceFirst("..", dir);
			}
		}		
		return argArray;
	}
}

/* vim :set ts=4 tw=4 tws=4 */
