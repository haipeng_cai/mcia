/**
 * File: src/distEA/distdistEAOptions.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 02/05/15		hcai		created; for command-line argument processing for distEA Instrumenter
 * 02/18/15		hcai		added option for intercepting NIOs at synchronous mode while by default we treat asynchronous mode
 * 02/27/15		hcai		added options for intercepting specified types of net i/o vehicles
*/
package distEA;

import java.util.ArrayList;
import java.util.List;

import EAS.EAOptions;

public class distEAOptions  extends EAOptions {
	
	public final static int OPTION_NUM = EAOptions.OPTION_NUM + 2;
	private boolean sync_nio_probes = false;
	public boolean probe_sync_nio() { return sync_nio_probes; }
	private boolean per_thread_monitor = false;
	public boolean monitor_per_thread() { return per_thread_monitor; }
	private boolean use_socket = false;
	public boolean use_socket() { return use_socket; }
	private boolean use_nio = false;
	public boolean use_nio() { return use_nio; }
	
	private boolean use_objstream = false;
	public boolean use_objstream() { return use_objstream; }
	
	public String[] process(String[] args) {
		args = super.process(args);
		
		List<String> argsFiltered = new ArrayList<String>();
		
		for (int i = 0; i < args.length; ++i) {
			String arg = args[i];

			if (arg.equals("-syncnio")) {
				// by default, we handle asynchronous non-blocking I/O for intercepting java NIO based network traffic, unless specified as 'sync' mode here  
				sync_nio_probes = true;
			}
			else if (arg.equals("-socket")) {
				use_socket = true;
			}
			else if (arg.equals("-objstream")) {
				use_objstream = true;
			}
			else if (arg.equals("-nio")) {
				use_nio = true;
			}
			else if (arg.equals("-perthread")) {
				// by default, we monitor inter-process message passing; when this is set, do for inter-thread message passing instead  
				per_thread_monitor = true;
			}
			else {
				argsFiltered.add(arg);
			}
		}
		
		String[] arrArgsFilt = new String[argsFiltered.size()];
		return (String[]) argsFiltered.toArray(arrArgsFilt);
	}
}

/* vim :set ts=4 tw=4 tws=4 */

