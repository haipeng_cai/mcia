# Import all classification package

import numpy
import random
import os
import sys
import string

import inspect, re

#from classes.sample import Sample
import pickle
import copy

if __name__=="__main__":
    if len(sys.argv)<3:
        print >> sys.stdout, "two few arguments."
        sys.exit(-1)

    basicfh = file (sys.argv[1],'r')
    allbasiclines = basicfh.readlines()
    basicfh.close()
    basicKeys = set()
    for line in allbasiclines:
        line = line.lstrip().rstrip()
        preitems = string.split(line, '\"')
        if len(preitems)<2:
            items = string.split(line,',')
        else:
            items = [preitems[1]] + string.split(preitems[2], ',')
        if len (items) < 9:
            continue
        basicKeys.add ( items[0]+items[6] )


    impfh = file (sys.argv[2],'r')
    allimplines = impfh.readlines()
    impfh.close()
    impKeys = set()
    for line in allimplines:
        line = line.lstrip().rstrip()
        preitems = string.split(line, '\"')
        if len(preitems)<2:
            items = string.split(line,',')
        else:
            items = [preitems[1]] + string.split(preitems[2], ',')
        if len (items) < 9:
            continue
        impKeys.add ( items[0]+items[6] )


    commonKeys = basicKeys.intersection( impKeys )

    fhnewbasic = file (sys.argv[1]+'.txt','w')

    for line in allbasiclines:
        line = line.lstrip().rstrip()
        items = string.split(line, ',')
        if len (items) < 9:
            continue
        key = items[0] + items[6]
        if key in commonKeys:
            print >> fhnewbasic, "%s\t%s" % (key, '\t'.join( items[1:len(items)] ))

    fhnewbasic.close()

    fhnewimp = file (sys.argv[2]+'.txt','w')
    for line in allimplines:
        line = line.lstrip().rstrip()
        items = string.split(line, ',')
        if len (items) < 9:
            continue
        key = items[0] + items[6]
        if key in commonKeys:
            print >> fhnewimp, "%s\t%s" % (key, '\t'.join( items[1:len(items)] ))

    fhnewimp.close()


    sys.exit(0)

# hcai: set ts=4 tw=120 sts=4 sw=4
