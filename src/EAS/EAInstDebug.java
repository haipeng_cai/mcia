/**
 *
 * File: src/EAS/EAInstDebug.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 06/20/13		hcai		based on EAInst.java, add debug-wise code for examining Soot constructs
 * 
*/
package EAS;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import profile.InstrumManager;

import dua.Extension;
import dua.Forensics;
import dua.global.ProgramFlowGraph;
import dua.global.dep.DependenceFinder;
import dua.method.CFG;
import dua.method.CFG.CFGNode;
import dua.method.CFGDefUses.NodeDefUses;
import dua.method.CFGDefUses.Use;
import dua.method.CFGDefUses.Variable;
import dua.method.CallSite;
import dua.unit.StmtTag;
import dua.util.Util;
import fault.StmtMapper;

import soot.*;
import soot.JastAddJ.FinallyHost;
import soot.JastAddJ.TryStmt;
import soot.dava.Dava;
import soot.jimple.*;
import soot.tagkit.LineNumberTag;
import soot.tagkit.Tag;
import soot.toolkits.graph.Block;
import soot.toolkits.graph.BlockGraph;
import soot.toolkits.graph.ExceptionalBlockGraph;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.toolkits.graph.UnitGraph;
import soot.util.*;

public class EAInstDebug implements Extension {
	
	private SootClass clsMonitor;
	
	private SootMethod mInitialize;
	private SootMethod mEnter;
	private SootMethod mReturnInto;
	private SootMethod mTerminate;
	
	File fJimpleOrig = null;
	File fJimpleInsted = null;
	
	static boolean bProgramStartInClinit = false;
	
	public static void main(String args[]){
		args = preProcessArgs(args);

		EAInstDebug eaInst = new EAInstDebug();
		Forensics.registerExtension(eaInst);
		Forensics.main(args);
		
		// important warning concerning the later runs of the instrumented subject
		if (bProgramStartInClinit) {
			System.out.println("\n***NOTICE***: program start event probe has been inserted in EntryClass::<clinit>(), " +
					"the instrumented subject MUST thereafter run independently rather than via EARun!");
		}
	}
	
	private static String[] preProcessArgs(String[] args) {
		args = EAOptions.process(args);
		
		String[] argsForDuaF;
		int offset = 0;

		argsForDuaF = new String[args.length + 2 - offset];
		System.arraycopy(args, offset, argsForDuaF, 0, args.length-offset);
		argsForDuaF[args.length+1 - offset] = "-paramdefuses";
		argsForDuaF[args.length+0 - offset] = "-keeprepbrs";
		
		return argsForDuaF;
	}
	
	public void run() {
		System.out.println("Running EAS extension of DUA-Forensics");
		//StmtMapper.getCreateInverseMap();
		
		clsMonitor = Scene.v().getSootClass("EAS.Monitor");
		mInitialize = clsMonitor.getMethodByName("initialize");
		mEnter = clsMonitor.getMethodByName("enter");
		mReturnInto = clsMonitor.getMethodByName("returnInto");
		mTerminate = clsMonitor.getMethodByName("terminate");
		
		if (EAOptions.dumpJimple()) {
			fJimpleOrig = new File(Util.getCreateBaseOutPath() + "JimpleOrig.out");
			writeJimple(fJimpleOrig);
		}
		
		List<SootMethod> entryMes = ProgramFlowGraph.inst().getEntryMethods();
		List<SootClass> entryClses = null;
		if (EAOptions.sclinit()) {
			entryClses = new ArrayList<SootClass>();
			for (SootMethod m:entryMes) {
				entryClses.add(m.getDeclaringClass());
			}
		}
		
		/* traverse all classes */
		Iterator<SootClass> clsIt = Scene.v().getClasses().iterator();
		while (clsIt.hasNext()) {
			SootClass sClass = (SootClass) clsIt.next();
			if ( sClass.isPhantom() ) {
				// skip phantom classes
				continue;
			}
			if ( !sClass.isApplicationClass() ) {
				// skip library classes
				continue;
			}
			
			// when there is a static initializer in the entry class, we will instrument the monitor initialize in there instead of the entry method
			boolean hasCLINIT = false;
			if (EAOptions.sclinit()) {
				if (entryClses.contains(sClass)) {
					try {
						SootMethod cl = sClass.getMethodByName("<clinit>");
						hasCLINIT = (cl != null);
					}
					catch (Exception e) {
						// if exception was thrown, either because there are more than one such method (ambiguous) or there is none
						hasCLINIT = (e instanceof RuntimeException && e.toString().contains("ambiguous method"));
					}
				}
			}
			
			/* traverse all methods of the class */
			Iterator<SootMethod> meIt = sClass.getMethods().iterator();
			while (meIt.hasNext()) {
				SootMethod sMethod = (SootMethod) meIt.next();
				if ( !sMethod.isConcrete() ) {
					// skip abstract methods and phantom methods, and native methods as well
					continue; 
				}
				if ( sMethod.toString().indexOf(": java.lang.Class class$") != -1 ) {
					// don't handle reflections now either
					continue;
				}
				
				// cannot instrument method event for a method without active body
				if ( !sMethod.hasActiveBody() ) {
					continue;
				}
				
				//Body body = sMethod.getActiveBody();
				Body body = sMethod.retrieveActiveBody();
				
				/* the ID of a method to be used for identifying and indexing a method in the event maps of EAS */
				String meId = sClass.getName() +	"::" + sMethod.getName();
				// -- DEBUG
				if (EAOptions.debugOut()) {
					System.out.println("\nNow instrumenting method " + meId + "...");
				}
				
				PatchingChain<Unit> pchn = body.getUnits();
				//Chain<Local> locals = body.getLocals();
				
				/* 1. instrument method entry events and program start event */
				CFG cfg = ProgramFlowGraph.inst().getCFG(sMethod);

				List<CFGNode> cfgnodes = cfg.getFirstRealNonIdNode().getSuccs();
				/* the first non-Identity-Statement node is right where we instrument method entrance event */
				CFGNode firstNode = cfg.getFirstRealNonIdNode(), firstSuccessor = cfgnodes.get(0);
				Stmt firstStmt = firstNode.getStmt();
				/*
				List<CFGNode> cfgnodes = cfg.getNodes();
				for (CFGNode curnode : cfgnodes) {
					if (curnode.isSpecial()) {
						// reached the EXIT node of the CFG
						break;
					}
					Stmt stmt = curnode.getStmt();
					if (stmt instanceof IdentityStmt) {
						// cannot instrument before Id stmt
						continue;
					}
				}
				*/

				List<Stmt> enterProbes = new ArrayList<Stmt>();
				List<StringConstant> enterArgs = new ArrayList<StringConstant>();
				
				boolean bInstProgramStart = false;
				if (EAOptions.sclinit()) {
					bInstProgramStart = (hasCLINIT && sMethod.getName().equalsIgnoreCase("<clinit>")) || 
														(!hasCLINIT && entryMes.contains(sMethod));
					if (!bProgramStartInClinit) {
						bProgramStartInClinit = (hasCLINIT && sMethod.getName().equalsIgnoreCase("<clinit>"));
					}
				}
				else {
					bInstProgramStart = entryMes.contains(sMethod);
				}
				
				// when "-sclinit" option specified, instrument monitor initialize in <clinit> if any, otherwise in entry method
				if ( bInstProgramStart ) {
					// before the entry method executes, the monitor needs be initialized
					List<StringConstant> initializeArgs = new ArrayList<StringConstant>();
					Stmt sInitializeCall = Jimple.v().newInvokeStmt( Jimple.v().newStaticInvokeExpr(
							mInitialize.makeRef(), initializeArgs ));
					enterProbes.add(sInitializeCall);
					
					// -- DEBUG
					if (EAOptions.debugOut()) {
						System.out.println("monitor initialization instrumented at the beginning of Entry method: " + meId);
					}
				} // -- if (entryMes.contains(sMethod))
				
				enterArgs.add(StringConstant.v(meId));
				Stmt sEnterCall = Jimple.v().newInvokeStmt( Jimple.v().newStaticInvokeExpr(
						mEnter.makeRef(), enterArgs ));
				enterProbes.add(sEnterCall);
				
				// -- DEBUG
				if (EAOptions.debugOut()) {
					System.out.println("monitor enter instrumented at the beginning of application method: " + meId);
				}
				if ( firstStmt != null ) {
					InstrumManager.v().insertBeforeNoRedirect(pchn, enterProbes, firstStmt);
				}
				else {
					InstrumManager.v().insertProbeAtEntry(sMethod, enterProbes);
				}
				
				/* 2. instrument method returned-into events and program termination event */
				/*
				List<Stmt> retinProbes = new ArrayList<Stmt>();
				List<StringConstant> retinArgs = new ArrayList<StringConstant>();
				retinArgs.add(StringConstant.v(meId));
				Stmt sReturnIntoCall = Jimple.v().newInvokeStmt( Jimple.v().newStaticInvokeExpr(
						mReturnInto.makeRef(), retinArgs ));
				retinProbes.add(sReturnIntoCall);
				*/
				
				/*
				for (CFGNode curnode : cfgnodes) {
					Stmt stmt = curnode.getStmt();
					
					// 2.1: normal returns
					if (curnode.hasAppCallees()) {
						assert stmt instanceof InvokeStmt;
						
						InstrumManager.v().insertAfter(pchn, retinProbes, stmt);
					}
					
					// 2.2: in a catch block
					else if (curnode.isInCatchBlock()) {
						Block bb = Util.getBB(stmt);
						
						InstrumManager.v().insertAfter(pchn, retinProbes, (Stmt)bb.getHead());
					}
					// 2.3: in a finally block
				}
				*/
				
				// 2.1 normal returns from call sites OUTSIDE any traps (exception catchers) 
				List<CallSite> callsites = cfg.getCallSites();
				int nCandCallsites = 0;
				for (CallSite cs : callsites) {
					if (cs.hasAppCallees() && !cs.isInCatchBlock()) {
						nCandCallsites ++;
					}
				}
				boolean havingFinally = false;
				String bodySource = body.toString();
				if (bodySource.contains("throw") && 
					bodySource.contains(":= @caughtexception") &&
					bodySource.contains("catch java.lang.Throwable from")) {
					// a very naive, probably imprecise but safe, way to determine if there is any finally block in this method
					havingFinally = true;
					System.out.println("finally block exists in method: " + meId);
				}
				
				int nCurCallsite = 0;
				for (CallSite cs : callsites) {
					if (!cs.hasAppCallees()) {
						// only care about application calls
						continue;
					}
					
					if (cs.isInCatchBlock()) {
						// for catch blocks, we always instrument at the beginning of each block, without looking into
						// the call sites inside the block
						continue;
					}
					
					nCurCallsite ++;
					// -- DEBUG
					if (EAOptions.debugOut()) {
						System.out.println("monitor returnInto instrumented at call site " +
								cs + " in method: " + meId);
					}
					
					List<Stmt> retinProbes = new ArrayList<Stmt>();
					List<StringConstant> retinArgs = new ArrayList<StringConstant>();
					retinArgs.add(StringConstant.v(meId));
					retinArgs.add(StringConstant.v(/*cs.getLoc().toString()*/
							cs.hashCode() + ": returnInto after calling " + cs.getAppCallees().get(0).getName()));
					Stmt sReturnIntoCall = Jimple.v().newInvokeStmt( Jimple.v().newStaticInvokeExpr(
							mReturnInto.makeRef(), retinArgs ));
					retinProbes.add(sReturnIntoCall);
					
					// before the entry method finishes, method events are to be dumped
					// -- the exit point of the entry method is regarded as the program end point

					// if there is no any finally block in the entry method, we probe for monitor terminate after the last call site
					if (!havingFinally && nCurCallsite==nCandCallsites && entryMes.contains(sMethod)) {	
						List<StringConstant> terminateArgs = new ArrayList<StringConstant>();
						terminateArgs.add(StringConstant.v(/*cs.getLoc().toString()*/
								cs.hashCode() + ": terminate after calling " + cs.getAppCallees().get(0).getName()));
						Stmt sTerminateCall = Jimple.v().newInvokeStmt( Jimple.v().newStaticInvokeExpr(
								mTerminate.makeRef(), terminateArgs ));
						//retinProbes.add(sTerminateCall);
						
					    // -- DEBUG
						if (EAOptions.debugOut()) {
							System.out.println("monitor terminate instrumented at last app call site " +
									cs + " in method: " + meId);
						}
					}
					InstrumManager.v().insertAfter(pchn, retinProbes, cs.getLoc().getStmt());
					/*
					InstrumManager.v().insertAtProbeBottom(pchn, retinProbes, cs.getLoc().getStmt());
					InstrumManager.v().insertRightBeforeNoRedirect(pchn, retinProbes, cs.getLoc().getStmt());
					*/
				}
				
				BlockGraph bg = new ExceptionalBlockGraph(body);
				//List<Block> exitBlocks = bg.getTails();
				Map<Stmt, Stmt> cbExitStmts = new LinkedHashMap<Stmt, Stmt>(); // a map from head to exit of each catch block
				for (Block block : bg.getBlocks()) {
					//block.getTail();
					Stmt head = (Stmt)block.getHead(), curunit = head, exit = curunit;
					//if ( ProgramFlowGraph.inst().getNode((head)).isInCatchBlock() ) { // exception blocks are not handled in DuaF for now
					if ( head.toString().contains(":= @caughtexception") ) {	
						while (curunit != null) {
							exit = curunit;
							curunit = (Stmt)block.getSuccOf(curunit);
						}
						cbExitStmts.put(head, exit);
					}
				}
				
				/*
				UnitGraph ug = new ExceptionalUnitGraph(body);
				List<Unit> exitUnits = ug.getTails();
				for (Unit exitPnt : exitUnits) {
					// -- DEBUG
					if (EAOptions.debugOut()) {
						System.out.println("monitor returnedInto instrumented at " + 
								exitPnt + " in method: " + meId);
					}
					
					InstrumManager.v().insertBeforeNoRedirect(pchn, retinProbes, (Stmt)exitPnt);
				}
				*/
								
				// 2.2 catch blocks and finally blocks
				Chain<Trap> traps = body.getTraps();
				if (traps.size() < 1) {
					// no catchers, no catch block instrumentation
					//continue;
				}
				
				/*
				for (Unit u : pchn) {
					Stmt s = (Stmt)u;
					for (Tag tag : s.getTags()) {
						if (tag instanceof LineNumberTag) {
							LineNumberTag ltag = (LineNumberTag) tag;
							ltag.getLineNumber();
							// we could read the source file using this source code line number
							if (mSource.contains("try") && mSource.contains("finally")) {
					
							}
						}
					}
				}
				*/
				
				// record instrumentation targets to avoid repetitive instrumentation
				Set<Unit> instTargets = new LinkedHashSet<Unit>();
				for (Iterator<Trap> trapIt = traps.iterator(); trapIt.hasNext(); ) {
				    Trap trap = trapIt.next();

				    // instrument at the beginning of each catch block
				    // -- DEBUG
					if (EAOptions.debugOut()) {
						System.out.println("monitor returnInto instrumented at the beginning of catch block " +
								trap + " in method: " + meId);
					}
					
					List<Stmt> retinProbes = new ArrayList<Stmt>();
					List<StringConstant> retinArgs = new ArrayList<StringConstant>();
					retinArgs.add(StringConstant.v(meId));
					retinArgs.add(StringConstant.v(/*trap.toString()*/
							trap.hashCode() + ": returnInto in catch block for " + trap.getException().getName()));
					Stmt sReturnIntoCall = Jimple.v().newInvokeStmt( Jimple.v().newStaticInvokeExpr(
							mReturnInto.makeRef(), retinArgs ));
					retinProbes.add(sReturnIntoCall);
					
					/*
					if (entryMes.contains(sMethod)) {	
						List<StringConstant> terminateArgs = new ArrayList<StringConstant>();
						terminateArgs.add(StringConstant.v(trap.toString()));
						Stmt sTerminateCall = Jimple.v().newInvokeStmt( Jimple.v().newStaticInvokeExpr(
								mTerminate.makeRef(), terminateArgs ));
						retinProbes.add(sTerminateCall);
						// -- DEBUG
						if (EAOptions.debugOut()) {
							System.out.println("monitor terminate instrumented at catch block " +
									trap + " in method: " + meId);
						}
					}
					*/
					
				    //InstrumManager.v().insertBeforeNoRedirect(pchn, retinProbes, (Stmt)trap.getHandlerUnit());
					Stmt instgt = (Stmt)trap.getHandlerUnit();
					if (!instTargets.contains(instgt)) {
						InstrumManager.v().insertAfter (pchn, retinProbes, instgt);
						instTargets.add(instgt);
					}
					
					// instrument at the beginning of each finally block if any
					if (havingFinally) {
						if (trap.getHandlerUnit().equals(trap.getEndUnit())) {
							// we don't need instrument the probes at the same point twice
							// -- note that for a same TRY statement, the beginning of the finally block, which is trap.getEndUnit(),
							// might be equal to the beginning of a catch block
							continue;
						}
						/* whenever there is a finally block for a TRY statement (maybe without any catch block),
						 *   the block will be copied to the end of every trap; So we can just instrument at the end of
						 *   each of these traps to meet the requirement of instrumenting in each finally block
						 */
						List<Stmt> retinProbesF = new ArrayList<Stmt>();
						List<StringConstant> retinArgsF = new ArrayList<StringConstant>();
						retinArgsF.add(StringConstant.v(meId));
						retinArgsF.add(StringConstant.v(/*trap.toString() */
								trap.getEndUnit().hashCode() + ": returnInto in finally block for " + trap.getException().getName()));
						Stmt sReturnIntoCallF = Jimple.v().newInvokeStmt( Jimple.v().newStaticInvokeExpr(
								mReturnInto.makeRef(), retinArgsF ));
						retinProbesF.add(sReturnIntoCallF);
						// -- DEBUG
						if (EAOptions.debugOut()) {
							System.out.println("monitor returnInto instrumented at the beginning of the finally block " +
									trap.getEndUnit() + " in method: " + meId);
						}
						/* -- when there is a finally block in the entry method, it is complicated to place the probe for the 
						 * termination event correctly so that it is to be always the actual terminating point and, to be 
						 * invoked exactly once -- we need then instrument at the end of the last finally block if it is at the
						 * end of the method (namely no app calls afterwards)
						 *  
						if (entryMes.contains(sMethod)) {
							List<StringConstant> terminateArgsF = new ArrayList<StringConstant>();
							terminateArgsF.add(StringConstant.v("In finally block: " + trap.getEndUnit().toString()));
							Stmt sTerminateCallF = Jimple.v().newInvokeStmt( Jimple.v().newStaticInvokeExpr(
									mTerminate.makeRef(), terminateArgsF ));
							retinProbesF.add(sTerminateCallF);
							// -- DEBUG
							if (EAOptions.debugOut()) {
								System.out.println("monitor terminate instrumented at finally block " +
										trap.getEndUnit() + " in method: " + meId);
							}
						}
						*/
						// again, avoid repetitive instrumentation although it does not affect the algorithm's correctness of EAS
						if (!((Stmt)trap.getEndUnit()).toString().contains("EAS.Monitor: void returnInto")) {
							InstrumManager.v().insertBeforeRedirect (pchn, retinProbesF, (Stmt)trap.getEndUnit());
						}
					}
					else if (entryMes.contains(sMethod)) {
						// if there is no finally block in the entry method, we can safely insert the probe for termination event
						// after the end of each catch block
						List<Stmt> terminateProbes = new ArrayList<Stmt>();
						List<StringConstant> terminateArgs = new ArrayList<StringConstant>();
						terminateArgs.add(StringConstant.v(/*trap.toString()*/
								trap.hashCode() + ": terminate in catch block for " + trap.getException().getName()));
						Stmt sTerminateCall = Jimple.v().newInvokeStmt( Jimple.v().newStaticInvokeExpr(
								mTerminate.makeRef(), terminateArgs ));
						terminateProbes.add(sTerminateCall);
						// -- DEBUG
						if (EAOptions.debugOut()) {
							System.out.println("monitor terminate instrumented after the end of catch block " +
									trap + " in method: " + meId);
						}
						// if a monitor probe has already become the end unit, we should insert this termination probe after that point
						if (((Stmt)trap.getEndUnit()).toString().contains("EAS.Monitor: void returnInto")) {
							//InstrumManager.v().insertAfter (pchn, terminateProbes, (Stmt)trap.getEndUnit());
						}
						// otherwise, just insert right before the last unit of the catch block
						else {
							/*
							//Stmt tgt = (Stmt)trap.getUnitBoxes().get(trap.getUnitBoxes().size()-1).getUnit();
							//InstrumManager.v().insertBeforeRedirect (pchn, terminateProbes, tgt);
							Stmt tgt = (Stmt)trap.getEndUnit();
							InstrumManager.v().insertRightBeforeNoRedirect (pchn, terminateProbes, tgt);
							*/
							Stmt cbHead = (Stmt)trap.getHandlerUnit(); // the handler unit is the beginning unit of the catch block
							assert cbExitStmts.containsKey(cbHead);
							Stmt tgt = cbExitStmts.get(cbHead);
							if (tgt instanceof GotoStmt /*&& !(tgt instanceof IdentityStmt)*/) {
								//InstrumManager.v().insertBeforeRedirect (pchn, terminateProbes, tgt);
							}
							else {
								//InstrumManager.v().insertAfter (pchn, terminateProbes, tgt);
							}
						}
					}
				}
				
				// Thus far, we have not insert probe for termination event in the entry method if there is any finally blocks in there
				if (entryMes.contains(sMethod) /*&& havingFinally*/) {
					Set<Stmt> fTargets = new LinkedHashSet<Stmt>();
					Stmt lastThrow = null;
					for (Unit u : pchn) {
						Stmt s = (Stmt)u;
						try {
						NodeDefUses node = (NodeDefUses) ProgramFlowGraph.inst().getNode(s);
						List<Variable> defs = node.getDefinedVars();
						List<Variable> uses = node.getUsedVars();
						System.out.println(defs);
						System.out.println(uses);
						for (Variable def:defs) {
							List<Use> udefs = DependenceFinder.getAllUsesForDef(def, node);
							System.out.println(udefs);
						}
						}
						catch (Exception e) {
						}
						
						
						if (s instanceof AssignStmt) {
							AssignStmt _s = (AssignStmt)s;
 							Value lv = _s.getLeftOp();
							if (lv instanceof Local) {
								int a = 1, b;
								b = a;
								Type lt = ((Local)lv).getType();
								if (lt instanceof RefLikeType)
								System.out.println( lt );
							}
							else {
								int a = 2, b;
								b = a;
							}
							Value rv = _s.getRightOp();
							if (rv instanceof Local) {
								int a = 1, b;
								b = a;
								Type rt = lv.getType(); 
								if (rt instanceof RefLikeType)
								System.out.println( rt );
							}
							else {
								int a = 2, b;
								b = a;
								if (rv instanceof InstanceFieldRef) {
									String h = ((InstanceFieldRef)rv).getBase().toString();
									System.out.println(h + ";" + ((InstanceFieldRef)rv).getBase().getType());
								}
							}
  							if (s.containsArrayRef()) {
								ArrayRef av = _s.getArrayRef();
							}
							if (s.containsFieldRef()) {
								FieldRef fv = _s.getFieldRef();
							}
							if (s.containsInvokeExpr()) {
								InvokeExpr iv = _s.getInvokeExpr();
							}
						}
						if (havingFinally) {
							// if the finally block is to be executed after an uncaught exception was thrown, a throw statement 
							// will be the exit point; However, we only probe at the last such finally block if there is no any app call site following it  
							if (s instanceof ThrowStmt) {
								//System.out.println("Got a throw statement " + s);
								lastThrow = s;
							}
							if (lastThrow != null) {
								if (ProgramFlowGraph.inst().getNode(s).hasAppCallees()) {
									lastThrow = null;
								}
							}
						}
						
						// In Jimple IR, any method has exactly one return statement
						if (dua.util.Util.isReturnStmt(s)) {
							fTargets.add(s);
						}

						/* Try statement and Finally block identification are  NOT supported with Jimple IR 
						if (s instanceof TryStmt) {
							System.out.println("Got a Try statement " + s);
						}
						if (s instanceof FinallyHost) {
							System.out.println("Got a Finally block " + s);
						}
						*/
					}
					if (lastThrow != null) {
						//fTargets.add(lastThrow);
					}
					assert fTargets.size() >= 1;
					for (Stmt tgt : fTargets) {
						List<Stmt> terminateProbe = new ArrayList<Stmt>();
						List<StringConstant> terminateArgs = new ArrayList<StringConstant>();
						String flag =  (dua.util.Util.isReturnStmt(tgt)?"return":"throw") + " statement";
						terminateArgs.add(StringConstant.v("terminate in finally block before " + flag));
						Stmt sTerminateCall = Jimple.v().newInvokeStmt( Jimple.v().newStaticInvokeExpr(
								mTerminate.makeRef(), terminateArgs ));
						terminateProbe.add(sTerminateCall);
						// -- DEBUG
						if (EAOptions.debugOut()) {
							System.out.println("monitor termination instrumented at the end of the finally block in " + 
									meId + " before " + flag);
						}
						InstrumManager.v().insertBeforeRedirect (pchn, terminateProbe, tgt);
					}
				}
				
			} // -- while (meIt.hasNext()) 
			
		} // -- while (clsIt.hasNext())
		
		if (EAOptions.dumpJimple()) {
			fJimpleInsted = new File(Util.getCreateBaseOutPath() + "JimpleInstrumented.out");
			writeJimple(fJimpleInsted);
		}
	
	} // -- void run()
	
	private static void writeJimple(File fObj) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(fObj));
			
			/* traverse all classes */
			Iterator<SootClass> clsIt = Scene.v().getClasses().iterator();
			while (clsIt.hasNext()) {
				SootClass sClass = (SootClass) clsIt.next();
				if ( sClass.isPhantom() ) {
					// skip phantom classes
					continue;
				}
				if ( !sClass.isApplicationClass() ) {
					// skip library classes
					continue;
				}
				
				/* traverse all methods of the class */
				Iterator<SootMethod> meIt = sClass.getMethods().iterator();
				while (meIt.hasNext()) {
					SootMethod sMethod = (SootMethod) meIt.next();
					if ( !sMethod.isConcrete() ) {
						// skip abstract methods and phantom methods, and native methods as well
						continue; 
					}
					if ( sMethod.toString().indexOf(": java.lang.Class class$") != -1 ) {
						// don't handle reflections now either
						continue;
					}
					
					// cannot instrument method event for a method without active body
					if ( !sMethod.hasActiveBody() ) {
						continue;
					}
					
					//Body body = sMethod.getActiveBody();
					Body body = sMethod.retrieveActiveBody();
					writer.write("\t"+sClass.getName()+"\n");
					writer.write(body + "\n");
				}
			}
			writer.flush();
			writer.close();
		}
		catch (FileNotFoundException e) { System.err.println("Couldn't write Jimple file: " + fObj + e); }
		catch (SecurityException e) { System.err.println("Couldn't write Jimple file: " + fObj + e); }
		catch (IOException e) { System.err.println("Couldn't write Jimple file: " + fObj + e); }
	}
	
} // -- public class EAInstDebug

/* vim :set ts=4 tw=4 tws=4 */

