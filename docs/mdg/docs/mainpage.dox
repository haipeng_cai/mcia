/** 
\mainpage Abstracting Program Dependencies using the Method Dependence Graph

Traditional dependence model based on the system dependence graph (SDG) supports fine-grained dependence analysis yet
often becomes too heavyweight to be applicable. The <b>Method Dependence Graph (MDG)</b> is developed for abstracting 
the statement-level dependencies in the SDG to method (function) level directly to provide a lighter-weight dependence 
model for application analyses working at method level. A representative example of such application analyses is 
impact analysis, which commonly works at method level thus a method-level dependence model suffices.

Given a program, our technique builds its MDG which includes full method-level data and control dependencies, including
those due to exceptional control flows. As its focus is on dependencies among methods, the MDG does not include 
statement-level dependencies. Instead, intraprocedural dependencies are abstracted away using dependence summaries
inside methods.

This is the project homepage for MDG, where the usage information is hosted and how MDG works is illustrated by
an example program and the MDG diagram of the program. The MDG tool is realized on top of our data-flow analysis and
instrumentation library <span style="color:blue;font-weight:bold"><a href="http://www3.nd.edu/~rsanteli/duaf/">DUA-forensics</a></span>, which is based on the 
<span style="color:blue;font-weight:bold"><a href="http://www.sable.mcgill.ca/soot/">Soot byte-code analysis and optimization framework</a></span>. Both MDG and 
DUA-forensics can be downloaded, with other third-party libraries, using the links provided on this page.

\section mdg_howworks How MDG works
- \subpage page_usage
- \subpage page_illustration

\section mdg_downloads Downloads
- <a href="../MDG.jar"> Download MDG </a><br>
- <a href="../DUA-forensics.jar"> Download DUA-forensics </a><br>
- <a href="../libs.jar"> Download all other libraries required </a><br>

*/

