/** 
\mainpage Diver: Precise Dynamic Impact Analysis using Dependence-based Trace Pruning

<b>Diver</b>, short for <B><I>D</I></B>ynamic <B><I>i</I></B>mpact analysis by tracking <B><I>v</I></B>alue transf<B><I>er</I></B>s, is a tool 
that is designed for precisely predicting the impacts of proposed changes to Java software systems at a time and space 
cost that is practically acceptable. What unlies Diver is a fine-grained program dependence graph, using with the technique prunes methods 
that executed after the given query but have no data/control dependence on that query.

This is the project homepage for Diver, where the tool implementation and information about the usage are hosted. The Diver tool is built on top of 
our data-flow analysis and instrumentation library <span style="color:blue;font-weight:bold"><a href="http://www3.nd.edu/~rsanteli/duaf/">DUA-forensics</a></span>, 
which is based on the <span style="color:blue;font-weight:bold"><a href="http://www.sable.mcgill.ca/soot/">Soot byte-code analysis and optimization framework</a></span>. 
Both Diver and DUA-forensics, also with other third-party libraries, can be downloaded using the links provided on this page.

\note An implementation of the <span style="color:blue;font-weight:bold"><a href="http://dl.acm.org/citation.cfm?id=1062534">Execute-After Sequence (EAS)</a></span> is also included in the Diver package. In particular, <b>our EAS implementation corrected the one presented in the original paper</b> by capturing method returned-into events for methods throwing exceptions that are not handled by either themselves or all of its runtime callers, and methods on the call stack when that happens, which would all be missed by the CollectEA---the original EAS implementation.

\section diver_howworks How Diver works
- \subpage page_usage
- \subpage page_illustration

\section diver_downloads Downloads
- <a href="../Diver.jar"> Download Diver </a><br>
- <a href="../DUA-forensics.jar"> Download DUA-forensics </a><br>
- <a href="../libs.jar"> Download all other libraries required </a><br>

*/

