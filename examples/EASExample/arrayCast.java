
import java.io.*;
import java.util.*;

public class arrayCast
{
	static class A {
		int a;
		float b;

		public A(int _a, float _b) {
			a = _a;
			b = _b;
		}

	}
	static class B extends A {
		double d;

		public B(int _a, float _b, double _d) {
			super(_a,_b);
			d = _d;
		}
	}

	static void upCast1() {
		B[] b = new B[] { new B(1,2f,4d) };
		A[] a = (A[]) b;

		System.out.println("upCast1 before A[0].a=" + a[0].a);
		b[0].a = 2;
		System.out.println("upCast1 after A[0].a=" + a[0].a);
	}
	/*
	static void upCast2() {
		int[] b = new int[10];
		long[] a = (long[]) b;
	}
	*/

	static void downCast1() {
		A[] b = new A[10];
		B[] a = (B[]) b;
	}
	/*
	static void downCast2() {
		long[] b = new long[10];
		int[] a = (int[]) b;
	}
	*/

	public static void main(String[] args) {
		upCast1();
		//upCast2();

		//downCast1();
		//downCast2();
		String a = new String("abc");
		a = "cdf";
		System.out.println(a);

		System.out.println("program ended.");
	}
}

