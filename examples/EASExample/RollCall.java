/* the working example designed for developing the mcia techniques */ 

import java.io.*;
import java.util.*;

import profile.DUAReporter;
import EAS.Monitor;

public class RollCall {
	static void __link() { DUAReporter.__link(); Monitor.__link(); }

	static class Student {
		public String name;
		public float tuition;
		public char grade;

		public Student(String _name, float _tuition, char _grade) {
			name = _name;
			tuition = _tuition;
			grade = _grade;
		}
	}

	static String g_result = "";
	double dmh = 0.5;
	public RollCall() {
		g_result = "abc";
		dmh = 1.2;
	}

	private int multiReturns() {
		float a=1.0f, b=2.0f;

		dmh = 20.25;

		Student s = new Student("a", 12.12f, 'D');
		float d = s.tuition;

		if (a > b) return 2;
		if (a + b < 20.0f) return 1;
		if (a + b > 20.0f) return 0;
		if (a - b > 20.0f) return -1;
		if (a - b < 20.0f) return -2;
		return (a<b)?1000:-1000;
	}

	static void M1() {
		int[] a = new int[] {12,3,-5};
		int b = a[1] + 2;

		Student[] s = new Student[] {new Student("Jone", 9.15f, 'E')};
		getBonus(s[0]);
	}

	static char cropGrade(char g, int i) {
		char r = g;
		if (i>6 && r < 'E') r = 'E';
		return r;
	}

	static int getBonus(Student s) {
		int r = 0;
		if (s.name.startsWith("J")) r += 10;
		if (s.grade < 'B') r -= 5;
		return r;
	}

	static boolean checkTuitionAndGrade(Student s, boolean b) {
		int u = getBonus(s);
		float t = s.tuition;
		s.grade = cropGrade(s.grade, u);
		if (b) { return t>10.5; }
		return t>=8.5;
	}

	static boolean validateName(String name) throws Exception {
		if (name.indexOf("&*")!=-1) {
		//Chg2: if (name.indexOf("*")!=-1)
		  throw new Exception("invalid characters included");
		}
		return name.length()>=1;
	}

	public static int generalEnroll(Student s) {
		int ret = 0;
		//Chg1  (addition of a statement):  s.grade = 'B' (in mcia, concrete change is irrelevant)
		try {
			if (!validateName(s.name)) ret = -1;
		}
		catch (Exception e) {
			return -2;
		}
		finally {
			ret = partialEnroll(s);
		}
		return ret;
	}

	public static int partialEnroll(Student s) {
		boolean e = false; 
		if (!checkTuitionAndGrade(s, e)) return -1;
		return 0;
	}

	public static void main(String[] args) {
		Student s = new Student("Jone", 9.15f, 'E');
		int n = 0;
		if (args.length >=1 && args[0] == "F") {
			n = partialEnroll (s);
		}
		else {
			n = generalEnroll (s);
		}
		g_result = "L" + n;
		System.out.println("Rollcall process results in: " + g_result);

		float a=1.0f, b=2.0f;
		if (a > b) return;
		if (a + b < 20.0f) return ;
		if (a + b > 20.0f) return ;
		if (a - b > 20.0f) return ;
		if (a - b < 20.0f) return ;
		return ;
	}
}

