#!/bin/bash

ROOT=$HOME
subjectloc=`pwd`
MAINCLASS=RollCall

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/tools/DUAForensics-bins-code/InstrReporters:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar"

rm -f ${MAINCLASS}*.class  ${MAINCLASS}.class 
rm -rf $subjectloc/bin
mkdir -p $subjectloc/bin
javac -g:source -source 1.4 -cp ${MAINCP} -d $subjectloc/bin ${MAINCLASS}.java

mkdir -p out-EAInstr

SOOTCP=".:$ROOT/software/j2re1.4.2_18/lib/rt.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin"

OUTDIR=$subjectloc/EAInstrumented
rm -rf $OUTDIR
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
java -Xmx1600m -ea -cp ${MAINCP} EAS.EAInst \
	-w -cp ${SOOTCP} \
	-p cg verbose:true,implicit-entry:false -p cg.spark verbose:true,on-fly-cg:true,rta:true \
	-f c -d "$OUTDIR" -brinstr:off -duainstr:off \
   	-duaverbose \
	-slicectxinsens \
	-debug \
	-dumpJimple \
	-allowphantom \
	-main-class $MAINCLASS -entry:$MAINCLASS \
	-process-dir $subjectloc/bin  \
	1>out-EAInstr/instr.out 2>out-EAInstr/instr.err
stoptime=`date +%s%N | cut -b1-13`
echo "StaticAnalysisTime elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Instrumentation finished."

###############################################################################################

INDIR=$subjectloc/EAInstrumented

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$INDIR"

OUTDIR=$subjectloc/EAoutdyn
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
java -Xmx2800m -ea -cp ${MAINCP} EAS.EARun \
	$MAINCLASS \
	"$subjectloc" \
	"$INDIR" \
	"" \
	$OUTDIR \
	"-fullseq" 

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Running finished."

###############################################################################################

NT=${1:-""}

INDIR=$subjectloc/EAoutdyn

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$INDIR"

starttime=`date +%s%N | cut -b1-13`
	#"main" \
	#"generalEnroll" \
java -Xmx2800m -ea -cp ${MAINCP} EAS.EAAnalysis \
	"CheckTuitionAndGrade" \
	"$INDIR" \
	$NT

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Analyzing finished."

exit 0

