#!/bin/bash

ROOT=$HOME
subjectloc=`pwd`
MAINCLASS=C

function compile()
{
	MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar:$subjectloc/src/"

	rm -f ${MAINCLASS}*.class  ${MAINCLASS}.class 
	rm -rf $subjectloc/bin
	mkdir -p $subjectloc/bin
	javac -g:source -source 1.4 -cp ${MAINCP} -d $subjectloc/bin $subjectloc/src/${MAINCLASS}.java
}

function instr()
{
	mkdir -p out-DiverInstr
	MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar"

	#$ROOT/software/j2re1.4.2_18/lib/rt.jar
	SOOTCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$subjectloc/bin"

	OUTDIR=$subjectloc/DiverInstrumented
	rm -rf $OUTDIR
	mkdir -p $OUTDIR

	starttime=`date +%s%N | cut -b1-13`
		#-p jb use-original-names:true \
	java -Xmx1600m -ea -cp ${MAINCP} Diver.DiverInst \
		-w -cp ${SOOTCP} \
		-p cg verbose:true,implicit-entry:false -p cg.spark verbose:true,on-fly-cg:true,rta:true \
		-f c -d "$OUTDIR" -brinstr:off -duainstr:off \
		-duaverbose \
		-slicectxinsens \
		-debug \
		-wrapTryCatch \
		-dumpJimple \
		-allowphantom \
		-serializeVTG \
		-main-class $MAINCLASS -entry:$MAINCLASS \
		-process-dir $subjectloc/bin  \
		1>out-DiverInstr/instr.out 2>out-DiverInstr/instr.err
	stoptime=`date +%s%N | cut -b1-13`
	echo "StaticAnalysisTime elapsed: " `expr $stoptime - $starttime` milliseconds

	echo "Instrumentation finished."
}

###############################################################################################

function run()
{
	INDIR=$subjectloc/DiverInstrumented

	MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$INDIR"

	OUTDIR=$subjectloc/Diveroutdyn
	mkdir -p $OUTDIR

	starttime=`date +%s%N | cut -b1-13`
	java -Xmx2800m -ea -cp ${MAINCP} Diver.DiverRun \
		$MAINCLASS \
		"$subjectloc" \
		"$INDIR" \
		"" \
		$OUTDIR 

	stoptime=`date +%s%N | cut -b1-13`
	echo "RunTime for elapsed: " `expr $stoptime - $starttime` milliseconds

	echo "Running finished."
}

###############################################################################################
function analysis()
{
	NT=${1:-"1"}

	INDIR=$subjectloc/Diveroutdyn
	BINDIR=$subjectloc/DiverInstrumented

	MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$INDIR"

	starttime=`date +%s%N | cut -b1-13`
		#"main" \
		#"generalEnroll" \
	java -Xmx2800m -ea -cp ${MAINCP} Diver.DiverAnalysis \
		"M1,M2,M3,M4,M5,M6,M7,main" \
		"$INDIR" \
		"$BINDIR" \
		$NT \
		-debug

	stoptime=`date +%s%N | cut -b1-13`
	echo "RunTime elapsed: " `expr $stoptime - $starttime` milliseconds

	echo "Analyzing finished."
}

#instr;

#run;

analysis;

exit 0

