import profile.BranchReporter;
import profile.DUAReporter;
import Diver.EAMonitor;
import EAS.Monitor;

/* example code for Diver */

public class C {
	static void __link() { 
		BranchReporter.__link(); DUAReporter.__link(); 
		Monitor.__link(); EAMonitor.__link(); 
	}
	
	public static boolean M5(A q) {
		long y = q.d;
		boolean b = B.t > y;
		q.d = 2;
		return b;
	}

	public static void main(String[] args) {
		int a = 0, b = -3;
		String s = new A().M1(a,b);
		double d = B.M4();
		String u = s + d;
		//System.out.println(u);
	}
}

